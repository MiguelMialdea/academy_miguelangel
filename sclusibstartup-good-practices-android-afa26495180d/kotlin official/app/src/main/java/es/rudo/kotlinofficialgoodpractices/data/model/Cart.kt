package es.rudo.kotlinofficialgoodpractices.data.model

import java.io.Serializable

class Cart : Serializable {
    var currency: String? = "eur"
    var total: Double = 0.0
    var items: ArrayList<CartItem> = ArrayList()
    var customer_id: String? = null
}