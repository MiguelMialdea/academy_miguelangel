package es.rudo.kotlinofficialgoodpractices.api

object Config {
    const val API_URL = "https://chatripp-staging.rudo.es/"
    const val API_URL_STRIPE = "http://translator.rudo.es/"
}