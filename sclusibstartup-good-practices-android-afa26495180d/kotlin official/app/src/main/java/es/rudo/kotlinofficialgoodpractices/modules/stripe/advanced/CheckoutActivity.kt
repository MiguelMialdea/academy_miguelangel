package es.rudo.kotlinofficialgoodpractices.modules.stripe.advanced

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.Observable
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import com.google.gson.GsonBuilder
import com.stripe.android.*
import com.stripe.android.model.*
import es.rudo.kotlinofficialgoodpractices.R
import es.rudo.kotlinofficialgoodpractices.data.model.Cart
import es.rudo.kotlinofficialgoodpractices.databinding.ActivityCheckoutBinding
import es.rudo.kotlinofficialgoodpractices.helpers.Constants
import java.lang.ref.WeakReference


class CheckoutActivity : AppCompatActivity() {

    private lateinit var binding: ActivityCheckoutBinding
    private lateinit var viewModel: CheckoutViewModel

    private lateinit var stripe: Stripe
    private lateinit var paymentSession: PaymentSession
    private var isShippingEnabled = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_checkout)
        viewModel = ViewModelProvider(this).get(CheckoutViewModel::class.java)

        binding.lifecycleOwner = this
        binding.checkoutActivity = this
        binding.checkoutViewModel = viewModel

        if (intent.getSerializableExtra("cart") != null) {
            viewModel.cart = intent.getSerializableExtra("cart") as Cart
        }

        stripe = Stripe(
            applicationContext,
            Constants.STRIPE_PUBLISHABLE_KEY
        )

        PaymentConfiguration.init(
            applicationContext,
            Constants.STRIPE_PUBLISHABLE_KEY
        )

        CustomerSession.initCustomerSession(this, EphemeralKeyProvider(this, "adrian@rudo.es"))
        paymentSession = PaymentSession(
            this,
            PaymentSessionConfig.Builder().setShippingMethodsRequired(false)
                .setShippingInfoRequired(false).build()
        )
        paymentSession.init(object : PaymentSession.PaymentSessionListener {
            override fun onCommunicatingStateChanged(isCommunicating: Boolean) {
                if (isCommunicating) {
                    binding.buttonPayments.isEnabled = false
                    binding.buttonPay.isEnabled = false
                    binding.buttonShipping.isEnabled = false
                } else {
                    binding.buttonPayments.isEnabled = true
                    binding.buttonPay.isEnabled = true
                    binding.buttonShipping.isEnabled = true
                }
            }

            override fun onError(errorCode: Int, errorMessage: String) {

            }

            override fun onPaymentSessionDataChanged(data: PaymentSessionData) {
                viewModel.paymentMethod = data.paymentMethod
                if (data.isPaymentReadyToCharge) {
                    viewModel.requestPaymentIntent(viewModel.paymentMethod?.customerId)
                    binding.buttonPay.isEnabled = true
                    binding.buttonPayments.text =
                        "Tarjeta acabada en " + data.paymentMethod?.card?.last4
                }
            }
        })

        initObservers()
    }

    private fun initObservers() {
        viewModel.isPaymentSuccess.observe(this, {
            it.let {
                finish()
            }
        })
        viewModel.showError.observe(this, {
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        })
    }

    /**
     * REQUEST PAY TO STRIPE
     */
    fun payOrder() {
        stripe.confirmPayment(
            this,
            ConfirmPaymentIntentParams.createWithPaymentMethodId(
                viewModel.paymentMethod?.id.toString(),
                viewModel.paymentIntentClientSecret
            )
        )
    }

    /**
     * OPENS CARD SELECTOR FOR STRIPE CUSTOMER
     */
    fun openPaymentMethods() {
        paymentSession.presentPaymentMethodSelection()
    }

    /**
     * OPENS SHIPPING ADDRESS FLOW
     */
    fun openShippingFlow() {
        if (isShippingEnabled) {
            paymentSession.presentShippingFlow()
        } else {
            Toast.makeText(this, "Envíos deshabilitados", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            paymentSession.handlePaymentData(requestCode, resultCode, data)
        }
        val weakActivity = WeakReference<Activity>(this)
        stripe.onPaymentResult(requestCode, data, object : ApiResultCallback<PaymentIntentResult> {
            override fun onSuccess(result: PaymentIntentResult) {
                val paymentIntent = result.intent
                val status = paymentIntent.status
                if (status == StripeIntent.Status.Succeeded) {
                    val gson = GsonBuilder().setPrettyPrinting().create()
                    weakActivity.get()?.let { activity ->
                        Toast.makeText(applicationContext, "payment Suceeded", Toast.LENGTH_LONG)
                            .show()
                        result.intent.id.let {
                            viewModel.confirmPayment(it)
                        }
                    }
                } else if (status == StripeIntent.Status.RequiresPaymentMethod) {
                    weakActivity.get()?.let { activity ->
                        Toast.makeText(applicationContext, "payment Failed", Toast.LENGTH_LONG)
                            .show()
                    }
                }
            }

            override fun onError(e: Exception) {
                weakActivity.get()?.let { activity ->
                    Toast.makeText(applicationContext, "payment Failed", Toast.LENGTH_LONG).show()
                }
            }
        })
    }
}