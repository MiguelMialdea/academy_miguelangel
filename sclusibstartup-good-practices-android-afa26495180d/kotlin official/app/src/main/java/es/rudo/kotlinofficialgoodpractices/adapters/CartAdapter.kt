package es.rudo.kotlinofficialgoodpractices.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import es.rudo.kotlinofficialgoodpractices.data.model.CartItem
import es.rudo.kotlinofficialgoodpractices.databinding.ItemCartitemBinding

class CartAdapter(
    private val context: Context,
    private val clickListener: CartItemClickListener
) :
    ListAdapter<CartItem, CartAdapter.ViewHolder>(ListAdapterCallback()) {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position)!!, context, clickListener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    class ViewHolder private constructor(val binding: ItemCartitemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(
            item: CartItem,
            context: Context,
            clickListener: CartItemClickListener
        ) {
            binding.cartitem = item
            binding.clickListener = clickListener
            binding.executePendingBindings()

            binding.buttonAddItem.setOnClickListener {
                item.quantity++
                binding.itemQuantity.text = (item.quantity).toString()
                clickListener.onClick(item)
            }
            binding.buttonRemoveItem.setOnClickListener {
                if (item.quantity > 0) {
                    item.quantity--
                    binding.itemQuantity.text = (item.quantity).toString()
                    clickListener.onClick(item)
                }
            }
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemCartitemBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }

    class ListAdapterCallback : DiffUtil.ItemCallback<CartItem>() {
        override fun areItemsTheSame(oldItem: CartItem, newItem: CartItem): Boolean {
            return oldItem.name == newItem.name
        }

        @SuppressLint("DiffUtilEquals")
        override fun areContentsTheSame(oldItem: CartItem, newItem: CartItem): Boolean {
            return oldItem == newItem
        }
    }

    interface CartItemClickListener {
        fun onClick(item: CartItem)
    }
}