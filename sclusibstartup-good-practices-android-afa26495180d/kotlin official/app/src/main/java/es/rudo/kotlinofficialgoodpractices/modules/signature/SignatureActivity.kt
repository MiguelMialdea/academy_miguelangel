package es.rudo.kotlinofficialgoodpractices.modules.signature

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.williamww.silkysignature.views.SignaturePad.OnSignedListener
import es.rudo.kotlinofficialgoodpractices.R
import es.rudo.kotlinofficialgoodpractices.databinding.ActivitySignatureBinding


class SignatureActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySignatureBinding
    private lateinit var viewModel: SignatureViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_signature)
        viewModel = ViewModelProvider(this).get(SignatureViewModel::class.java)

        binding.lifecycleOwner = this
        binding.signatureActivity = this
        binding.signatureViewModel = viewModel

        initObservers()
        initSignatureModule()
    }

    private fun initSignatureModule() {
        binding.signaturePad.setOnSignedListener(object : OnSignedListener {
            override fun onStartSigning() {

            }

            override fun onSigned() {
                //Event triggered when the pad is signed
            }

            override fun onClear() {
                //Event triggered when the pad is cleared
            }
        })
    }

    private fun initObservers() {

    }
}