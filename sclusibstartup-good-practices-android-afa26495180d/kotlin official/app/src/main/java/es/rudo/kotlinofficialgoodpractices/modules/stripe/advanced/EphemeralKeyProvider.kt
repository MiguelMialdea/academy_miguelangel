package es.rudo.kotlinofficialgoodpractices.modules.stripe.advanced

import android.content.Context
import android.util.Log
import androidx.annotation.Size
import com.google.gson.JsonObject
import com.stripe.android.EphemeralKeyProvider
import com.stripe.android.EphemeralKeyUpdateListener
import es.rudo.kotlinofficialgoodpractices.api.RetrofitClient
import es.rudo.kotlinofficialgoodpractices.data.model.StripeIntent
import es.rudo.kotlinofficialgoodpractices.helpers.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response

class EphemeralKeyProvider @JvmOverloads constructor(
    context: Context,
    private val stripeAccountId: String? = null
) : EphemeralKeyProvider {

    override fun createEphemeralKey(
        @Size(min = 4) apiVersion: String,
        keyUpdateListener: EphemeralKeyUpdateListener
    ) {
        var stripeIntent = StripeIntent()
        stripeIntent.stripe_version = apiVersion
//        stripeIntent.stripe_account = stripeAccountId//TODO GET FROM CACHE OR OTHER SOURCE
        stripeIntent.stripe_account = "adrian@rudo.es"

        CoroutineScope(Dispatchers.IO).launch {
            RetrofitClient().apiCall({
                RetrofitClient().createEphemeralKey(stripeIntent)
            },
                object : RetrofitClient.RemoteEmitter {
                    override fun onResponse(response: Response<Any>) {
                        if (response.code() == Constants.SERVER_SUCCESS_CODE) {
                            keyUpdateListener.onKeyUpdate(response.body().toString())
                        } else {
                            keyUpdateListener
                                .onKeyUpdateFailure(0, "error creating ephemeral key")
                        }
                    }

                    override fun onError(errorType: RetrofitClient.ErrorType, msg: String) {
                        Log.e("Api errortype", errorType.toString())
                        Log.e("Api message", msg)
                    }
                })
        }
    }
}
