package es.rudo.kotlinofficialgoodpractices.modules.capture_security

import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import es.rudo.kotlinofficialgoodpractices.R
import es.rudo.kotlinofficialgoodpractices.databinding.ActivityCaptureSecurityBinding


class CaptureSecurityActivity : AppCompatActivity() {

    private lateinit var binding: ActivityCaptureSecurityBinding
    private lateinit var viewModel: CaptureSecurityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window.setFlags(
            WindowManager.LayoutParams.FLAG_SECURE,
            WindowManager.LayoutParams.FLAG_SECURE
        )// THIS PREVENTS SCREENSHOTS AND RECORDING VIDEO OF THE ACTIVITY CONTENT

        binding = DataBindingUtil.setContentView(this, R.layout.activity_capture_security)
        viewModel = ViewModelProvider(this).get(CaptureSecurityViewModel::class.java)

        binding.lifecycleOwner = this
        binding.capturesecurityActivity = this
        binding.capturesecurityViewModel = viewModel

        Glide.with(this).load("https://www.53.com/content/dam/fifth-third/brand/card/cc-trio.jpg")
            .into(binding.imageCard)
    }
}