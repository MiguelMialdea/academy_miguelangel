package es.rudo.rudokotlinarchitecture.api.old

import com.google.gson.GsonBuilder
import es.rudo.rudokotlinarchitecture.data.model.Login
import es.rudo.rudokotlinarchitecture.api.Config
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

class ApiService {

    val clientWithoutAuth by lazy {
        Retrofit.Builder()
            .baseUrl(Config.API_URL)
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .build().create(es.rudo.rudokotlinarchitecture.api.Api::class.java)
    }

    interface Api {

        @GET("/rates")
        fun getRates(): Observable<List<String>>

        //LOGIN
        @POST
        suspend fun postLogin(@Body login: Login) : Response<Login>

    }
}