package es.rudo.rudokotlinarchitecture.modules.trips

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import es.rudo.rudokotlinarchitecture.data.model.City
import es.rudo.rudokotlinarchitecture.api.RetrofitClient
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class TripsViewModel : ViewModel() {

    private val retrofitClient: RetrofitClient =
        RetrofitClient()
    var cities = MutableLiveData<ArrayList<Any>>()

    // Trip onClick
    private var _navigateToTripDetail = MutableLiveData<Int>()
    val navigateToTripDetail: LiveData<Int>
        get() = _navigateToTripDetail

    init {
        getTrips()
    }

    private fun getTrips() {
        CoroutineScope(Dispatchers.IO).launch {
            val responseTrips = retrofitClient.getCities()
            withContext(Dispatchers.Main) {
                if (responseTrips.isSuccessful) {
                    cities.value = responseTrips.body()?.results as ArrayList<Any>?
                }
            }
        }
    }

    fun onTripClicked(id: Int) {
        _navigateToTripDetail.value = id
    }

}