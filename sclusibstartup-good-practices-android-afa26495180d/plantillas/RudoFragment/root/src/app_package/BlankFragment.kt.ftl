package ${escapeKotlinIdentifiers(packageName)}

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider

class ${fragmentClass}Fragment : Fragment() {


    private lateinit var binding: Fragment${fragmentClass}Binding
    private lateinit var viewModel: ${fragmentClass}ViewModel

    companion object {
        fun newInstance(): Fragment {
            return ${fragmentClass}Fragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_${fragmentLowercase}, container, false)
        viewModel = ViewModelProvider(this).get(${fragmentClass}ViewModel::class.java)

        val view = binding.root
        binding.lifecycleOwner = this
        binding.${fragmentLowercase}Fragment = this
        binding.${fragmentLowercase}ViewModel = viewModel

        initObservers()

        return view
    }
    
    private fun initObservers() {
    
    }
}
