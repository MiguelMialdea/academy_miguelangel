<?xml version="1.0"?>
<#import "root://activities/common/kotlin_macros.ftl" as kt>
<recipe>
    <@kt.addAllKotlinDependencies />

    <instantiate from="root/res/layout/fragment_blank.xml.ftl"
                   to="${escapeXmlAttribute(resOut)}/layout/${escapeXmlAttribute(fragmentName)}.xml" />

    <open file="${escapeXmlAttribute(resOut)}/layout/${escapeXmlAttribute(fragmentName)}.xml" />

    <instantiate from="root/src/app_package/BlankFragment.kt.ftl"
                   to="${escapeXmlAttribute(srcOut)}/${fragmentClass}Fragment.kt" />

    <open file="${escapeXmlAttribute(srcOut)}/${fragmentClass}Fragment.kt" />

    <instantiate from="root/src/app_package/BlankViewModel.kt.ftl"
                   to="${escapeXmlAttribute(srcOut)}/${fragmentClass}ViewModel.kt" />

    <open file="${escapeXmlAttribute(srcOut)}/${fragmentClass}ViewModel.kt" />
</recipe>