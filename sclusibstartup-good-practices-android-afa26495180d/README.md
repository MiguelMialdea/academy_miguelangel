
![rudo logo](https://rudo.es/bootcamp/rudo.png)


# good-practices-android
Proyecto de ejemplo con una estructuración estándar para Rudo que documenta buenas prácticas y ejemplos de uso.

Para ver un proyecto ejemplo hecho en java ir al siguiente enlace: https://bitbucket.org/sclusibstartup/good-practices-android/src/master/Java/
Para ver un proyecto ejemplo hecho en kotlin ir al siguiente enlace: 

## **Estructura del proyecto**

La carpeta base tendrá el nombre del proyecto, y dentro de la misma se incluirá:

- **Clase App:** Ésta extiende de Application y se incluirá la instancia, fabric y cualquier referencia que sea usada desde cualquier parte de la app.
- **Carpeta api:** Estructura de las llamadas y todo lo relacionado con la configuración y uso de retrofit.
- **Carpeta entities:** Aquí irán todas las clases que usaremos como objetos para manejar datos. Siempre poner las entidades en singular (ej: Profile).
- **Carpeta adapters:** Aquí estarán los controladores encargados de la lógica para mostrar
- **Carpeta ui:** Carpeta donde irán estructuradas por subcarpetas las actividades y fragments, así como su funcionalidad.
- **Carpeta utils:** Carpeta que incluirá Preferencias, constantes, request codes y todas las funcionalidades que no se puedan clasificar en otras carpetas.
- **Carpeta resources(res):** Aquí iran en subcarpetas las animaciones, drawables, fuentes, layouts, menús, etc.

Además de las carpetas citadas anteriormente tenemos los componentes AndroidManifest.xml y el Gradle. A continuación explicaremos más detalladamente los componentes principales:

### **AndroidManifest.xml**

Aquí se declaran todas las actividades que componen la app, además de los permisos y el estilo que tendrá la app base.

Será muy importante a la hora de crear un nuevo proyecto añadir las siguientes lineas:


```xml
android:name=".App"
android:networkSecurityConfig="@xml/network_security_config"
android:screenOrientation="portrait"
```

La primera de ellas nos permitirá usar la clase App para usarla para implementar la instancia global, fabric y demás cosas que queramos generar incluso
antes de que empiece la aplicación.

El networkSecurityConfig.xml servirá para poder hacer llamadas a api http(no cifradas) a partir de las versiones de Android N. La estructura del archivo será la siguiente:
```xml
<?xml version="1.0" encoding="utf-8"?>
<network-security-config>
    <domain-config cleartextTrafficPermitted="true">
        <domain includeSubdomains="true">yourdomain</domain>
    </domain-config>
</network-security-config>
```

Por último para que las vistas no se reinicien y se vean siempre en vertical (la mayoría de apps habrá que forzarlas por usabilidad) usaremos el screenorientation="portrait"


### **Gradle configuration**

Una de las cosas más importantes a la hora de crear un proyecto será configurar nuestro gradle.  Es recomendable al empezar un proyecto tener todas las dependencias posibles actualizadas, ya que así aseguraremos mayor compatibilidad con las últimas versiones Android y evitaremos problemas al actualizarlas, también usaremos siempre que podamos las librerías Androidx, ya que éstas sustituyen a las antiguas librerias support.

Ejemplo de gradle de proyecto:
```
buildscript {  
    repositories {  
        google()  
        jcenter()  
          
    }  
    dependencies {  
        classpath 'com.android.tools.build:gradle:3.5.0'  
  classpath 'com.google.gms:google-services:4.3.2'  
  // NOTE: Do not place your application dependencies here; they belong  
 // in the individual module build.gradle files  }  
}  
  
allprojects {  
    repositories {  
        google()  
        jcenter()  
        maven { url "https://jitpack.io" }  
    }  
}
```
Es importante ver que aquí hemos puesto primera dependencia google(), ya que el orden de importación de dependencias importa. También hemos añadido jcenter y maven para mayor compatibilidad con librerías ya sean de github o otros repositorios.

Por último en esta sección veremos el gradle del la app:

```xml
apply plugin: 'com.android.application'  
  
android {  
    compileSdkVersion 28  
  defaultConfig {  
        applicationId "es.rudo.chatripp"  
  minSdkVersion 21  
  targetSdkVersion 28  
  versionCode 1  
  versionName "1.0"  
  testInstrumentationRunner "androidx.test.runner.AndroidJUnitRunner"  
  }  
    buildTypes {  
        release {  
            minifyEnabled false  
  proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'  
  }  
    }  
    compileOptions {  
        sourceCompatibility = '1.8'  
  targetCompatibility = '1.8'  
  }  
}  
  
dependencies {  
    implementation fileTree(dir: 'libs', include: ['*.jar'])  
    implementation 'androidx.appcompat:appcompat:1.1.0'  
  implementation 'androidx.constraintlayout:constraintlayout:1.1.3'  
  implementation 'com.android.support:design:28.0.0'  
  implementation 'com.android.support:cardview-v7:28.0.0'  
  implementation 'com.android.support:support-v4:28.0.0'  
  implementation 'com.android.support:recyclerview-v7:28.0.0'  
  implementation 'com.android.support:appcompat-v7:28.0.0'  
  testImplementation 'junit:junit:4.12'  
  androidTestImplementation 'androidx.test:runner:1.2.0'  
  androidTestImplementation 'androidx.test.espresso:espresso-core:3.2.0'  
  
  //GOOGLE  
  implementation 'com.google.android.libraries.places:places:2.0.0'  
  
  //BUTTERKNIFE  
  implementation 'com.jakewharton:butterknife:10.1.0'  
  annotationProcessor 'com.jakewharton:butterknife-compiler:10.1.0'  
  
  //WEBSERVICES  
  implementation 'com.squareup.retrofit2:retrofit:2.6.1'  
  implementation 'com.squareup.retrofit2:converter-scalars:2.6.1'  
  implementation "com.squareup.retrofit2:converter-moshi:2.6.1"  
  implementation 'com.squareup.retrofit2:converter-gson:2.6.1'  
  implementation 'com.squareup.okhttp3:logging-interceptor:4.1.0'  
  implementation 'com.google.code.gson:gson:2.8.5'  
  implementation 'com.orhanobut:logger:2.2.0'  
  implementation 'com.github.ihsanbal:LoggingInterceptor:3.0.0'  
  
  //IMAGES  
  implementation 'com.squareup.picasso:picasso:2.71828'  
  implementation 'jp.wasabeef:picasso-transformations:2.2.1'  
  implementation 'com.github.bumptech.glide:glide:4.9.0'  
  implementation 'jp.wasabeef:glide-transformations:4.0.1'  
  annotationProcessor 'com.github.bumptech.glide:compiler:4.9.0'  
  implementation 'com.makeramen:roundedimageview:2.3.0'  
  implementation 'de.hdodenhof:circleimageview:3.0.0'  
  implementation 'com.github.dhaval2404:imagepicker-support:1.1'  
  implementation 'com.asksira.android:bsimagepicker:1.2.2'  
  implementation 'id.zelory:compressor:2.1.0'  
  implementation 'com.theartofdev.edmodo:android-image-cropper:2.7.0'  
  implementation 'com.github.yalantis:ucrop:2.2.3'  
  implementation 'com.bogdwellers:pinchtozoom:0.1'  
  
  //VIDEOS  
  implementation 'com.github.MasayukiSuda:Mp4Composer-android:v0.3.2'  
  implementation 'com.google.android.exoplayer:exoplayer:2.10.4'  
  implementation "im.ene.toro3:toro:3.6.2.2903"  
  implementation "im.ene.toro3:toro-ext-exoplayer:3.6.2.2903"  
  
  //OTHER  
  implementation 'com.github.prolificinteractive:material-calendarview:2.0.1'  
  implementation 'com.jakewharton.threetenabp:threetenabp:1.2.1'  
  implementation 'com.crystal:crystalrangeseekbar:1.1.3'  
  implementation "com.airbnb.android:lottie:3.0.7"  
  implementation 'io.fotoapparat:fotoapparat:2.7.0'  
  implementation 'com.otaliastudios:cameraview:2.0.0-beta06'  
  
  //FIREBASE  
  implementation 'com.google.firebase:firebase-core:17.2.0'  
  implementation 'com.google.firebase:firebase-messaging:20.0.0'  
  implementation 'com.google.firebase:firebase-auth:19.0.0'  
  implementation 'com.google.firebase:firebase-database:19.1.0'  
  implementation 'com.google.firebase:firebase-storage:19.0.1'  
  
}  
apply plugin: 'com.google.gms.google-services'
```
A destacar intentar siempre poner la compatibilidad con Androidx y compilar en la versión más reciente de gradle y sdk de Android. El resto de dependencias lo iremos añadiendo según sea necesario, nunca añadir más dependencias que las necesarias en el proyecto.

También destacaremos los siguientes parámetros que podemos cambiar:

 - VersionCode: Este será incremental de uno en uno y será el identificador interno para saber la versión de release de la app. Ésta deberá ser cambiada cada vez que subamos una versión a la play store.
 - VersionName: Este parámetro tan sólo será informativo para poder ver en la play store y en la app. Ejemplo: 1.0.1
 - ApplicationId: Este será el identificador único que reconocerá la play store de google para identificar nuestra app. Muy importante tenerla decidida y clara antes de subirla a la store ya que no puede haber otra igual. Ejemplos: es.rudo.intranet, com.chatripp.app

### **API**

Dentro de **API** se encuentran los ficheros necesarios para la configuración y estructura que se usará para realizar las llamadas de tipo Rest. Vamos a explicar rápidamente las más importantes:

 - Config: En este archivo configuraremos todas las variables como la *API_URL*, el *CLIENT_ID* y el *CLIENT_SECRET*, las cuales serán variables publicas y estáticas para su uso en toda laapp.
 - DataStrategy: Esta interfaz se encargará de listar las llamadas y las interfaces, así como los parámetros que se tienen que usar en cada llamada.
 - DataWebservice: En esta clase se realizará toda la lógica de retrofit usando las interfaces declaradas en *DataStrategy*. 
 - AccessTokenInterceptor: Esta clase sirve para añadir las cabeceras necesarias a las llamadas.
 - AccessTokenAuthenticator: Esta clase se usa para gestionar el refresco de token y autentificación con el servidor.


## **Cómo documentar un proyecto**

### **README.md**

Los README debería incluir información del proyecto que sea necesaria conocer para alguien que no lo ha tocado nunca. Sería interesante incluir un resumen de cómo está estructurado para encotrar todo fácilmente.

Para ver cómo se crear un README se puede ver el "código" de este mismo.

### **Funciones**

Todas las funciones que no sean las generales del proyecto deberían estar documentadas y deberían describir cómo funcionan, qué devuelven, y en caso de que sean muy complicadas, cómo se usan.

La declaración de la documentación de una función se puede hacer abriendo un comentario con una barra y dos asteriscos, y tiene que estar justo encima de la propia función.

Ejemplo de uso:
```java
/**  
 * This function returns a *Hello* string for a given name * 
 * @param name The name of the user  
 * @return A hello string to the user  
 */
 private String Hello(String name) {  
    String hello = "Hello, "+ name;  
 return hello;  
}
```

## **Declaración de elementos**

### **Ficheros**

Cada fichero debe nombrarse realacionada con su módulo o entidad. La nomenclatura utilizada será la siguiente:

 - Clase de tipo entidad: Estos deberán tener el nombre en **singular** del objeto al que se quiere representar. Por ejemplo Destination, Album, Profile, etc.
 - Clase de tipo actividad: Tendrá que empezar por el nombre de la pantalla que representará y acabado en Activity. Ejemplo: CameraActivity.
 - Clase de tipo fragment: Tendrá que empezar por el nombre de la pantalla que representará y acabado en Fragment. Ejemplo: TripsFragment.
 - Clase de tipo adapter: Tendrá que empezar por el nombre de la entidad que se quiere representar en **plural** y acabado en Adapter. Ejemplo: CitiesAdapter

### **Variables**

Las variables deben de ser declarados con un nombre lo más corto y aclarativo posible, empezando siempre con el tipo de elemento que es: textUsername, isLoading, etc.

Hay que evitar poner nombres genéricos a las variables. Hay que pensar que el nombre de una variable no debe de poder usarse para nombrar otro elemento.


### **Layouts, Iconos y imágenes **

Todos los assets relacionados con layouts, imagenes, iconos y  demás assets irán en la carpeta res. Todos los assets incluidos en esta carpeta tendrán que estar en minúsculas y sin símbolos raros como %,&, etc. A continuación hay ejemplos de diferentes nombres a poner en cada uno de los casos:

 - Los iconos en los assets se nombrarán con el siguiente fotmato: **ic_*nombre***.
 - Las imágenes en los assets se nombrarán con el siguiente formato: **img_*nombre***.
 - Los layouts se nombrarán primero por el tipo de vista que es y luego el nombre de la clase o asset que representa: **activity_login**,  **fragment_profile**, **item_album**, etc.

### **Funciones**

Las funciones deberán tener un nombre explciativo sobre lo que hace, sin importar que acabe con una declaración más larga de lo normal. Si es necesario se usarán las variables internas de la función en la declaración.

```java
private void sayHello(String name) {  
    System.out.println("Hello, " + name);  
}

sayHello("Tony Stark");
```

## **Braces**

A la hora de hacer if/else, hay que abrir y cerrar los braces siempre. Tan sólo se puede dar el caso especifico de no ponerlos sólo si fuera corto y cabiera en una línea:

**MAL**
```java
if (isFirstLoad)  
    if (isLoading){  
        // ...
  }else{
  // ...
  }  
// ...
```
**BIEN**
```java
if (something) {
            // ...
        } else if (somethingElse) {
            // ...
        } else {
            // ...
        }
```
```java
if (condition) body();
```
## **Shortcuts**

- **Ctrl+ alt+ L:** Reformatear código
- **Shift dos veces:** Buscar en todo el proyecto.
- **alt + insert:** Insertar código (ejemplos: getters, setters, override, etc).
- **alt + o:** Override method.
- **ctrl + space:** Autocompletar código (útil para autocompletar funciones, etc).
- **alt + enter:** Realizar corrección rápida de proyecto (mostrar acciones de intención y correcciones rápidas)

**Listado entero de shortcuts:** -> https://developer.android.com/studio/intro/keyboard-shortcuts?hl=es-419

## **Recursos**

**Android good practices** -> https://github.com/ribot/android-guidelines/blob/master/project_and_code_guidelines.md
