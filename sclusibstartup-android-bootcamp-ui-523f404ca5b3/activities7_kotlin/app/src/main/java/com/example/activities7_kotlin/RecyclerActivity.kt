package com.example.activities7_kotlin

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.activities7_kotlin.adapter.user_adapter
import com.example.activities7_kotlin.databinding.RecyclerActivityBinding
import com.example.activities7_kotlin.entities.User
import kotlinx.android.synthetic.main.recycler_activity.*

class RecyclerActivity: AppCompatActivity() {

    private lateinit var binding: RecyclerActivityBinding
    private lateinit var viewModel: RecyclerViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.recycler_activity)
        viewModel = ViewModelProvider(this).get(RecyclerViewModel::class.java)

        binding.lifecycleOwner = this
        binding.recyclerActivity = this
        binding.recyclerViewModel = viewModel


        initRecycler()
        observer()
    }

    private fun observer() {
        viewModel.loadPosts()
        viewModel.posts.observe(this , Observer {
            it?.let { user_adapter.sub }
        })
    }

    fun initRecycler(){
        recycler_users.layoutManager = LinearLayoutManager(this)
        val adapter = user_adapter(users)
        recycler_users.adapter = adapter
    }


}