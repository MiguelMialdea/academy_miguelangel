package com.example.activities7_kotlin

import android.content.Intent
import android.os.Bundle
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.activities7_kotlin.databinding.RegisterActivityBinding
import kotlinx.android.synthetic.main.register_activity.*
import java.util.regex.Matcher
import java.util.regex.Pattern

class RegisterActivity: AppCompatActivity() {

    private lateinit var binding: RegisterActivityBinding
    private lateinit var viewModel: RegisterViewModel
    var name: Boolean = false
    var password: Boolean = false
    var email: Boolean = false
    var terms: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.register_activity)
        viewModel = ViewModelProvider(this).get(RegisterViewModel::class.java)

        binding.lifecycleOwner = this
        binding.registerActivity = this
        binding.registerViewModel = viewModel

        textUnderlined()

        click()
    }

    private fun click() {
        binding.textLogin.setOnClickListener {
            if (edit_username.text.toString().isEmpty()) {
                edit_username.setError("Inserte datos")
            } else {
                if (edit_username.length() < 2){
                    edit_username.setError("Requiere minimo 2 letras")
                } else {
                    name = true
                }
            }

            if (edit_email.text.toString().isEmpty()){
                edit_email.setError("Inserte datos")
            } else {
                // Email pattern
                emailValidation(edit_email.text.toString())
            }
            if (edit_password.text.toString().isEmpty()) {
                edit_password.error = "Inserte datos"
            } else {
                // Password pattern
                passwordValidation(edit_password.text.toString())
            }
            if (edit_confirm_password.getText().toString().isEmpty()) {
                edit_confirm_password.setError("Inserte datos")
            } else {
                // Password repeated confirmation
                if (!edit_confirm_password.getText().toString()
                        .equals(edit_password.text.toString(), ignoreCase = true)
                ) {
                    edit_confirm_password.setError("Las claves no coinciden")
                } else {
                    password = true
                }
            }

            // Checkbox comprobation

            // Checkbox comprobation
            if (check_terms.isChecked() != true) {
                toast("Debes aceptar los terminos y condiciones")
            } else {
                terms = true
            }
            if (email && password && name && terms) {
                changeAct()
            }
        }
    }

    private fun changeAct() {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
    }

    private fun passwordValidation(password: String) {
        val pattern: Pattern
        val matcher: Matcher
        val PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,20}$"
        pattern = Pattern.compile(PASSWORD_PATTERN)
        matcher = pattern.matcher(password)

        if (!matcher.matches()) {
            edit_password.error =
                "La password requiere 8 caracteres, 1 mayúscula, 1 minúscula y 1 número"
        } else {
            email = true
        }
    }

    private fun toast(message: String) {
        val toast = Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT)
        toast.show()

    }

    private fun emailValidation(email: String) {
        val email_pattern: String = ("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$")

        val inputStr: CharSequence = email

        val pattern: Pattern = Pattern.compile(email_pattern, Pattern.CASE_INSENSITIVE)
        val matcher: Matcher = pattern.matcher(inputStr)

        if (!matcher.matches()){
            edit_email.setError("El email no es valido")
        }

    }

    private fun textUnderlined() {
        val check_text = SpannableString("Acepto terminos y condiciones")

        check_text.setSpan(UnderlineSpan(), 0, binding.checkTerms.length(), 0)

        binding.checkTerms.text = check_text
    }
}