package com.example.activities7_kotlin.entities

data class User(val name: String, val image: Int)