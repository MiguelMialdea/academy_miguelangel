package com.example.activities7_kotlin.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.activities7_kotlin.R
import com.example.activities7_kotlin.entities.User
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_user.view.*

class user_adapter(val list: List<User>) : RecyclerView.Adapter<user_adapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)

        return ViewHolder(layoutInflater.inflate(R.layout.item_user, parent, false))
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.render(list[position])
    }

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        fun render(list: User){
            view.text_username_item.text = list.name
            Picasso.get().load(list.image).into(view.image_user_item)
        }

    }

}