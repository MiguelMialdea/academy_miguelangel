package com.example.activities7_kotlin

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.activities7_kotlin.entities.User

class RecyclerViewModel(application: Application): AndroidViewModel(application) {

    var posts: MutableLiveData<List<User>> = MutableLiveData()

    fun loadPosts(){
        posts.value = listOf(
                User("Carlos", R.drawable.image1),
                User("Miguel", R.drawable.image2),
                User("David", R.drawable.image3),
                User("Manuel", R.drawable.image4),
                User("Ricardo", R.drawable.image5),
                User("Francesco", R.drawable.image6),
                User("Leticia", R.drawable.image7),
                User("Ruben", R.drawable.image8),
                User("Andrea", R.drawable.image9),
                User("Pepe", R.drawable.image1),
                User("Juanito", R.drawable.image1)
        )
    }

}