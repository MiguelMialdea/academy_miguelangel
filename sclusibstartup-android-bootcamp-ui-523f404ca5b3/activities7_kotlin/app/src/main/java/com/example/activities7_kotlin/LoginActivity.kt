package com.example.activities7_kotlin

import android.content.Intent
import android.os.Bundle
import android.text.SpannableString
import android.text.style.UnderlineSpan
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.activities7_kotlin.databinding.LoginActivityBinding
import kotlinx.android.synthetic.main.login_activity.*
import kotlinx.android.synthetic.main.register_activity.*

class LoginActivity: AppCompatActivity() {

    private lateinit var binding: LoginActivityBinding
    private lateinit var viewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.login_activity)
        viewModel = ViewModelProvider(this).get(LoginViewModel::class.java)

        binding.lifecycleOwner = this
        binding.loginActivity = this
        binding.loginViewModel = viewModel

        textUnderlined()
        click()
    }

    private fun click() {
        button_login.setOnClickListener {
            changeAct()
        }
    }

    private fun changeAct() {

        val intent = Intent(this, MenuActivity::class.java)
        startActivity(intent)
    }

    private fun textUnderlined() {
        val text_remember_underlined =
            SpannableString("¿No recuerdas tu contraseña?")
        val text_newAccount_underlined = SpannableString("¿Crea tu nueva cuenta?")
        val text_new_underlined = SpannableString("¿Eres nuevo?")

        text_remember_underlined.setSpan(UnderlineSpan(), 0, text_remember_underlined.length, 0)
        text_newAccount_underlined.setSpan(UnderlineSpan(), 0, text_newAccount_underlined.length, 0)
        text_new_underlined.setSpan(UnderlineSpan(), 0, text_new_underlined.length, 0)

        text_remember_login.setText(text_remember_underlined)
        text_newAccount_login.setText(text_newAccount_underlined)
        text_new_login.setText(text_new_underlined)
    }


}