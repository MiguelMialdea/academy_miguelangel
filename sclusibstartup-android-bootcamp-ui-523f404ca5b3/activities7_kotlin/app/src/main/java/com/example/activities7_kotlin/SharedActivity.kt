package com.example.activities7_kotlin

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.activities7_kotlin.databinding.SharedActivityBinding
import kotlinx.android.synthetic.main.shared_activity.*

class SharedActivity: AppCompatActivity() {

    private lateinit var binding: SharedActivityBinding
    private lateinit var viewModel: SharedViewModel
    private val MY_SHARED_PREF_NAME = "my_shared_pref_name"
    private val NAME = "name"
    private val AGE = "age"
    private val TICKET = "ticket"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.shared_activity)
        viewModel = ViewModelProvider(this).get(SharedViewModel::class.java)

        binding.lifecycleOwner = this
        binding.sharedActivity = this
        binding.sharedViewModel = viewModel

        getData()
        click()
    }

    private fun saveData() {
        val insertName = binding.editNameShared.text.toString()
        val insertAge = binding.checkShared.isChecked
        val insertTicket = binding.swTicketShared.isChecked

        val sharedPref = getSharedPreferences(MY_SHARED_PREF_NAME, Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        editor.putString(NAME, insertName)
        editor.putBoolean(AGE, insertAge)
        editor.putBoolean(TICKET, insertTicket)
        editor.apply()

        Toast.makeText(this, "Data saved", Toast.LENGTH_SHORT).show()
    }

    private fun click() {

        button_read_shared.setOnClickListener {
            getData()
        }

        button_delete_shared.setOnClickListener {
            val sharedPreferences = getSharedPreferences(MY_SHARED_PREF_NAME, Context.MODE_PRIVATE)

            sharedPreferences.edit().remove(NAME).apply()
            sharedPreferences.edit().remove(AGE).apply()
            sharedPreferences.edit().remove(TICKET).apply()

            binding.textUsernameShared.text = ""
            binding.textSwitchShared.text = ""
            binding.textCheckShared.text = ""
        }

        button_save_shared.setOnClickListener {
            saveData()
        }

    }

    private fun getData() {
        val sharedPreferences = getSharedPreferences(MY_SHARED_PREF_NAME, Context.MODE_PRIVATE)

        val name = sharedPreferences.getString(NAME, "")
        val age = sharedPreferences.getBoolean(AGE, false)
        val ticket = sharedPreferences.getBoolean(TICKET, false)

        binding.textUsernameShared.text = "$name"
        binding.textSwitchShared.text = "$ticket"
        binding.textCheckShared.text = "$age"
    }
}