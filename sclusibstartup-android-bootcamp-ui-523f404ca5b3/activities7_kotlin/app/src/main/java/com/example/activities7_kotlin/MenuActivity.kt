package com.example.activities7_kotlin

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.activities7_kotlin.databinding.MenuActivityBinding
import kotlinx.android.synthetic.main.menu_activity.*

class MenuActivity: AppCompatActivity() {
    private lateinit var binding: MenuActivityBinding
    private lateinit var viewModel: MenuViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.menu_activity)
        viewModel = ViewModelProvider(this).get(MenuViewModel::class.java)

        binding.lifecycleOwner = this
        binding.menuActivity = this
        binding.menuViewModel = viewModel

        click()
    }

    private fun click() {
        button_shared.setOnClickListener {
            val intent = Intent(this, SharedActivity::class.java)
            startActivity(intent)
        }
        button_recycler.setOnClickListener {
            val intent = Intent(this, RecyclerActivity::class.java)
            startActivity(intent)
        }
        button_sendFullData.setOnClickListener {
            val intent = Intent(this, SendDataActivity::class.java)
            startActivity(intent)
        }
    }
}