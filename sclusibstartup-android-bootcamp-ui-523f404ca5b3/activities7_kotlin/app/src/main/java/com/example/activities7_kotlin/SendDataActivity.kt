package com.example.activities7_kotlin

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.activities7_kotlin.databinding.SendDataActivityBinding

class SendDataActivity: AppCompatActivity() {

    private lateinit var binding: SendDataActivityBinding
    private lateinit var viewModel: SendDataViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.send_data_activity)
        viewModel = ViewModelProvider(this).get(SendDataViewModel::class.java)

        binding.lifecycleOwner = this
        binding.sendDataActivity = this
        binding.sendDataViewModel = viewModel
    }
}