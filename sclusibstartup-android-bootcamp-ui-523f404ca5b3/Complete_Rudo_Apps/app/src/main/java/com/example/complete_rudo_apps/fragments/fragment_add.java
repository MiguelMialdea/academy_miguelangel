package com.example.complete_rudo_apps.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.complete_rudo_apps.R;

public class fragment_add extends Fragment {

    EditText edit_name;
    Button button_save;
    View view;
    Fragment fragment;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_add, null);

        if (getActivity().getIntent().getBooleanExtra("EXIT", false)) {
            getActivity().finish();
        }

        edit_name = view.findViewById(R.id.edit_name);
        button_save = view.findViewById(R.id.button_save);

        setOnClickListeners();

        return view;
    }

    private void setOnClickListeners() {

        button_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edit_name.getText().toString().isEmpty()){
                    toast();
                }else{
                    loadFragment();
                }
            }
        });
    }

    private void loadFragment() {
        Bundle bundle = new Bundle();
        fragment = new fragment_add02();

        bundle.putString("name", edit_name.getText().toString());

        fragment.setArguments(bundle);
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, fragment);
        fragmentTransaction.addToBackStack(null);

        fragmentTransaction.commit();
    }



    private void toast() {
        Toast toast = Toast.makeText(getContext(), "Inserta un nombre",Toast.LENGTH_SHORT);
        toast.show();
    }
}
