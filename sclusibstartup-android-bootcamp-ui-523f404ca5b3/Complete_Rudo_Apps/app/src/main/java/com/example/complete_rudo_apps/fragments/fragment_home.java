package com.example.complete_rudo_apps.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Switch;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.complete_rudo_apps.R;

public class fragment_home extends Fragment {

    EditText edit_name;
    CheckBox check_age;
    Switch switch_ticket;
    Button button_save, button_delete, button_read;
    SharedPreferences shared;
    public static final String myPrefence = "mypref";
    public static final String name = "name";
    public static final String check = "check";
    public static final String sw = "switch";

    View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, null);

        edit_name = view.findViewById(R.id.edit_name);
        check_age = view.findViewById(R.id.check_Age);
        switch_ticket = view.findViewById(R.id.sw_ticket);
        button_save = view.findViewById(R.id.button_save);
        button_delete = view.findViewById(R.id.button_delete);
        button_read = view.findViewById(R.id.button_read);

        shared = this.getActivity().getSharedPreferences(myPrefence, Context.MODE_PRIVATE);

        setClickListeners();

        return view;
    }

    private void setClickListeners() {
        button_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String n = edit_name.getText().toString();
                Boolean a = check_age.isChecked();
                Boolean b = switch_ticket.isChecked();
                SharedPreferences.Editor editor = shared.edit();
                editor.putString(name, n);
                editor.putBoolean(check, a);
                editor.putBoolean(sw, b);
                editor.commit();
            }
        });

        button_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit_name.setText("");
                check_age.setChecked(false);
                switch_ticket.setChecked(false);
            }
        });

        button_read.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (shared.contains(name)){
                    edit_name.setText(shared.getString(name, ""));
                }

                if (shared.contains(check)){
                    check_age.setChecked(shared.getBoolean(check, false));
                }
                if (shared.contains(sw)){
                    switch_ticket.setChecked(shared.getBoolean(sw, false));
                }
            }
        });
    }

}
