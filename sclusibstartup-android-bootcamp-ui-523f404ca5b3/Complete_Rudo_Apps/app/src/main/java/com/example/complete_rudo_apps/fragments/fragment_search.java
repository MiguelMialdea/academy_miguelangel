package com.example.complete_rudo_apps.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.complete_rudo_apps.R;
import com.example.complete_rudo_apps.adapter.user_adapter;
import com.example.complete_rudo_apps.entities.User;

import java.util.ArrayList;

public class fragment_search extends Fragment {
    View view;
    RecyclerView recyclerView;
    ArrayList<User> list_user;
    user_adapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_search, container, false);
        list_user = new ArrayList<>();
        recyclerView = view.findViewById(R.id.recycler_users);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        loadUsers();
        adapter = new user_adapter(list_user);
        recyclerView.setAdapter(adapter);

        return view;
    }

    private void loadUsers() {
        list_user.add(new User("Carlos", R.drawable.image1));
        list_user.add(new User("Miguel", R.drawable.image2));
        list_user.add(new User("David", R.drawable.image3));
        list_user.add(new User("Manuel", R.drawable.image4));
        list_user.add(new User("Ricado", R.drawable.image5));
        list_user.add(new User("Francesco", R.drawable.image6));
        list_user.add(new User("Leticia", R.drawable.image7));
        list_user.add(new User("Ruben", R.drawable.image8));
        list_user.add(new User("Andrea", R.drawable.image9));
        list_user.add(new User("Pepe", R.drawable.image1));
        list_user.add(new User("Juanito",R.drawable.image1));
    }
}
