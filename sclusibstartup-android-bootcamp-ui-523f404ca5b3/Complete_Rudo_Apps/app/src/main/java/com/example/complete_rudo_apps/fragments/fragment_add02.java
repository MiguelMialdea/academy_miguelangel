package com.example.complete_rudo_apps.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.complete_rudo_apps.R;

public class fragment_add02 extends Fragment {
    String name;
    EditText edit_surname;
    TextView text_name;
    Button button_save;

    View view;
    Fragment fragment;
    Bundle bundle;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_add02, null);
        bundle();

        if (getActivity().getIntent().getBooleanExtra("EXIT", false)) {
            getActivity().finish();
        }

        edit_surname = view.findViewById(R.id.edit_surname);
        text_name = view.findViewById(R.id.text_name);
        button_save = view.findViewById(R.id.button_save);

        receiveData();

        setOnClickListeners();

        return view;
    }

    private void bundle() {
        bundle = getArguments();

        if (bundle == null){
            toast("No se han recibido los datos");
        }
    }

    private void receiveData() {
        name = bundle.getString("name");
        text_name.setText(name);
    }

    private void setOnClickListeners() {
        button_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edit_surname.getText().toString().isEmpty()){
                    toast("Inserta un apellido");
                }else{
                    loadFragment();
                }
            }
        });
    }

    private void loadFragment() {
        Bundle bundle = new Bundle();
        fragment = new fragment_add03();
        String completeName = name + " "+ edit_surname.getText().toString();

        bundle.putString("name", completeName);

        fragment.setArguments(bundle);
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, fragment);
        fragmentTransaction.addToBackStack(null);

        fragmentTransaction.commit();
    }

    private void toast(String message) {

        Toast toast = Toast.makeText(getContext(), message,Toast.LENGTH_SHORT);
        toast.show();
    }
}
