package com.example.complete_rudo_apps.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.complete_rudo_apps.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class fragment_login extends Fragment {

    Button button_login;
    EditText text_user, text_password;
    TextView text_remember, text_newAccount, text_new;
    String name, password, email;

    View view;
    Bundle bundle;
    Fragment fragment;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_login, container, false);
        bundle();

        button_login = view.findViewById(R.id.btnLogin);
        text_remember = view.findViewById(R.id.txtRemember);
        text_newAccount = view.findViewById(R.id.txtNewAccount);
        text_new = view.findViewById(R.id.text_new);
        text_user = view.findViewById(R.id.etUsername);
        text_password = view.findViewById(R.id.etpassword);

        initializeData();


        textUnderlined();
        setOnClickListeners();

        return view;
    }

    private void bundle() {
        bundle = getArguments();

        if (bundle == null){
            toast("No se han recibido los datos");
        }
    }

    private void initializeData() {
        name = bundle.getString("name");
        password = bundle.getString("password");
        email = bundle.getString("email");

        text_user.setText(name);
        text_password.setText(password);
    }

    private void setOnClickListeners() {
        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent();
            }
        });

        text_remember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toast("Recordar contraseña");
            }
        });

        text_newAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toast("Crear nueva cuenta");
            }
        });
    }

    private void intent() {
        sendData();

        //FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        //FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        //fragmentTransaction.replace(R.id.fragment_container, fragment);
        //fragmentTransaction.addToBackStack(null);

        //fragmentTransaction.commit();
    }

    private void sendData() {
        Bundle bundle = new Bundle();
        //fragment = new fragment_programming();

        bundle.putString("name", text_user.getText().toString());
        bundle.putString("email", email);

        fragment.setArguments(bundle);
    }

    private void textUnderlined() {
        SpannableString text_remember_underlined = new SpannableString("¿No recuerdas tu contraseña?");
        SpannableString text_newAccount_underlined = new SpannableString("¿Crea tu nueva cuenta?");
        SpannableString text_new_underlined = new SpannableString("¿Eres nuevo?");

        text_remember_underlined.setSpan(new UnderlineSpan(), 0, text_remember_underlined.length(), 0);
        text_newAccount_underlined.setSpan(new UnderlineSpan(), 0, text_newAccount_underlined.length(),0);
        text_new_underlined.setSpan(new UnderlineSpan(), 0, text_new_underlined.length(), 0);

        text_remember.setText(text_remember_underlined);
        text_newAccount.setText(text_newAccount_underlined);
        text_new.setText(text_new_underlined);
    }

    public void toast(String message){
        Toast toast = Toast.makeText(getContext(), message, Toast.LENGTH_SHORT);
        toast.show();
    }
}
