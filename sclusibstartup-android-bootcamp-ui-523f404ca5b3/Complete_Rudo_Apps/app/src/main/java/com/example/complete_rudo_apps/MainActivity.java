package com.example.complete_rudo_apps;

import android.os.Bundle;
import android.view.MenuItem;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;


import com.example.complete_rudo_apps.fragments.fragment_add;
import com.example.complete_rudo_apps.fragments.fragment_home;
import com.example.complete_rudo_apps.fragments.fragment_profile;
import com.example.complete_rudo_apps.fragments.fragment_search;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener{

    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        loadFragment(new fragment_home());

        bottomNavigationView = findViewById(R.id.navigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);

    }

    private Boolean loadFragment(androidx.fragment.app.Fragment fragment) {
        if (fragment != null){
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        androidx.fragment.app.Fragment fragment = null;

        switch (item.getItemId()){
            case R.id.navigation_home:
                fragment = new fragment_home();
                break;
            case R.id.navigation_search:
                fragment = new fragment_search();
                break;
            case R.id.navigation_add:
                fragment = new fragment_add();
                break;
            case R.id.navigation_profile:
                fragment = new fragment_profile();
                break;
        }
        return loadFragment(fragment);
    }
}