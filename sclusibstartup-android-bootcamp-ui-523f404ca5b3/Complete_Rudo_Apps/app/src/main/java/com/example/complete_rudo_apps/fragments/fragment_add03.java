package com.example.complete_rudo_apps.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.complete_rudo_apps.R;

public class fragment_add03 extends Fragment {
    String fullName;
    TextView text_fullName;
    Button button_finish;

    View view;
    Bundle bundle;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_add03, null);
        bundle();

        text_fullName = view.findViewById(R.id.text_fullname);
        button_finish = view.findViewById(R.id.btnFinish);

        fullName = bundle.getString("name");
        text_fullName.setText(fullName);

        setOnClickListeners();

        return view;
    }

    private void bundle() {
        bundle = getArguments();

        if (bundle == null){
            toast();
        }
    }

    private void setOnClickListeners() {
        button_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });
    }

    private void toast() {
        Toast toast = Toast.makeText(getContext(), "No se han enviado los datos",Toast.LENGTH_SHORT);
        toast.show();
    }
}
