package com.example.chatripp.entities;

public class City {
    int image;

    public City(int image) {
        this.image = image;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
