package com.example.chatripp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.chatripp.R;
import com.example.chatripp.entities.City;

import java.util.ArrayList;

public class city_adapter extends RecyclerView.Adapter<city_adapter.cityVH> {
    private ArrayList<City> list_city;

    public city_adapter(ArrayList<City> list_cities){
        this.list_city = list_cities;
    }

    @NonNull
    @Override
    public cityVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_city, null, false);
        return new cityVH(view);
    }

    @Override
    public void onBindViewHolder(@NonNull cityVH holder, int position) {
        holder.image.setImageResource(list_city.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return list_city.size();
    }

    public class cityVH extends RecyclerView.ViewHolder{
        ImageButton image;

        public cityVH(@NonNull View view) {
            super(view);
            image = view.findViewById(R.id.image_city);
        }
    }
}
