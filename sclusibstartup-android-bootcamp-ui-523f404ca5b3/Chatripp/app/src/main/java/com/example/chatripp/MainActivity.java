package com.example.chatripp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.MenuItem;

import com.example.chatripp.fragments.fragment_chat;
import com.example.chatripp.fragments.fragment_location;
import com.example.chatripp.fragments.fragment_notifications;
import com.example.chatripp.fragments.fragment_profile;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener{

    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadFragment(new fragment_location());

        bottomNavigationView = findViewById(R.id.navigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);

    }

    private Boolean loadFragment(Fragment fragment) {
        if (fragment != null){
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment fragment = null;

        switch (item.getItemId()){
            case R.id.navigation_location:
                fragment = new fragment_location();
                break;
            case R.id.navigation_chat:
                fragment = new fragment_chat();
                break;
            case R.id.navigation_profile:
                fragment = new fragment_profile();
                break;
            case R.id.navigation_notifications:
                fragment = new fragment_notifications();
                break;
        }
        return loadFragment(fragment);
    }
}