package com.example.chatripp.fragments;

import android.os.Bundle;
import android.telephony.RadioAccessSpecifier;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.chatripp.R;
import com.example.chatripp.adapter.city_adapter;
import com.example.chatripp.entities.City;

import java.util.ArrayList;

public class fragment_location extends Fragment {
    View view;
    RecyclerView recycler;
    ArrayList<City> list_cities;
    city_adapter adapter;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_location, null);

        list_cities = new ArrayList<>();
        recycler = view.findViewById(R.id.recycler_cities);
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));

        loadCities();

        adapter = new city_adapter(list_cities);
        recycler.setAdapter(adapter);

        return view;
    }

    private void loadCities() {
        list_cities.add(new City(R.drawable.madrid));
        list_cities.add(new City(R.drawable.pisa));
        list_cities.add(new City(R.drawable.rio_de_janeiro));
        list_cities.add(new City(R.drawable.verona));
        list_cities.add(new City(R.drawable.sydney));
        list_cities.add(new City(R.drawable.estepona));
        list_cities.add(new City(R.drawable.brasov));
        list_cities.add(new City(R.drawable.kalymnos));
        list_cities.add(new City(R.drawable.seville));
        list_cities.add(new City(R.drawable.filipstad));
        list_cities.add(new City(R.drawable.new_york));
        list_cities.add(new City(R.drawable.san_francisco));
        list_cities.add(new City(R.drawable.vatican_city));
    }
}
