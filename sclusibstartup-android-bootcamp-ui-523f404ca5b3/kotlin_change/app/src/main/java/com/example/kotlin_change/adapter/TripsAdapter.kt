package com.example.kotlin_change.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.kotlin_change.R
import com.example.kotlin_change.data.models.City

class TripsAdapter : RecyclerView.Adapter<TripsAdapter.ViewHolder>() {

    var data = listOf<City>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = data[position]
        holder.bind(item)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    class ViewHolder private constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val textCity: TextView = itemView.findViewById(R.id.text_city)
        private val imageCity: ImageView = itemView.findViewById(R.id.image_background)

        fun bind(item: City) {

            textCity.text = item.name
            Glide.with(itemView).load(item.medias?.midsize).into(imageCity)
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val view = layoutInflater
                    .inflate(R.layout.item_trip, parent, false)

                return ViewHolder(view)
            }
        }
    }

}