package com.example.kotlin_change.data.models

data class MyName(var name: String = "", var nickname: String = "")