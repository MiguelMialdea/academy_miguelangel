package com.example.kotlin_change.modules.chats

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.kotlin_change.api.RetrofitClient
import com.example.kotlin_change.helpers.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response

class ChatsViewModel: ViewModel() {

    val chats = MutableLiveData<Int>()

    init {
        CoroutineScope(Dispatchers.IO).launch {
            RetrofitClient().apiCall({
                RetrofitClient().getMe()
            },
                object : RetrofitClient.RemoteEmitter {
                    override fun onResponse(response: Response<Any>) {
                        if (response.code() == Constants.SERVER_SUCCESS_CODE){

                        }
                    }

                    override fun onError(errorType: RetrofitClient.ErrorType, msg: String) {
                        TODO("Not yet implemented")
                    }
                })
        }
    }
}