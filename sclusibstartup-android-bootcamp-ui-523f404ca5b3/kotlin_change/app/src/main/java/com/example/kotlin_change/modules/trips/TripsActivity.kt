package com.example.kotlin_change.modules.trips

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.kotlin_change.MainActivity
import com.example.kotlin_change.R
import com.example.kotlin_change.adapter.TripsPaginationAdapter
import com.example.kotlin_change.databinding.ActivityTripsBinding

class TripsActivity: AppCompatActivity() {

    private lateinit var binding: ActivityTripsBinding
    private lateinit var viewModel: TripsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_trips)
        viewModel = ViewModelProvider(this).get(TripsViewModel::class.java)

        binding.tripsViewModel = viewModel
        binding.tripsActivity = this

        val adapter = TripsPaginationAdapter(object : TripsPaginationAdapter.TripListener {
            override fun onClick(cityId: Int) {

            }

            override fun onDeleted(cityId: Int) {

            }
        })

        binding.recyclerTrips.adapter = adapter
        adapter.addLoading()

        viewModel.cities.observe(this, Observer {
            it?.let { adapter.submitList(it) }
        })

        viewModel.navigateToTripDetail.observe(this, Observer {
            it?.let {
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }
        })
    }
}