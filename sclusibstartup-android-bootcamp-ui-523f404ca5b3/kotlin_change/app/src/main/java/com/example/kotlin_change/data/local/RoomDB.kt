package com.example.kotlin_change.data.local

import android.content.Context
import androidx.room.*

@Database(
    entities = [DBLogin::class, DBUser::class],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun loginDao(): LoginDao
    abstract fun userDao(): UserDao

    companion object {
        @Volatile
        private var instance: AppDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also { instance = it }
        }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(
            context,
            AppDatabase::class.java, "app-database.db"
        )
            .build()
    }
}

@Dao
interface LoginDao {
    @Query("select * from DBLogin")
    fun getLogin(): DBLogin

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(login: DBLogin?)
}

@Dao
interface UserDao {
    @Query("select * from DBUser")
    fun getMe(): DBUser
}