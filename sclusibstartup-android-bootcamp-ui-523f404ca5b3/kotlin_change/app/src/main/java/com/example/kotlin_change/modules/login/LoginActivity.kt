package com.example.kotlin_change.modules.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.kotlin_change.R
import com.example.kotlin_change.databinding.ActivityLoginBinding
import com.example.kotlin_change.modules.trips.TripsActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity: AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding
    private lateinit var viewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        viewModel = ViewModelProvider(this).get(LoginViewModel::class.java)

        binding.lifecycleOwner = this
        binding.loginViewModel = viewModel

        viewModel.usernameError.observe(this, Observer<String> { error -> input_username.error = error })

        viewModel.passwordError.observe(this, Observer { error -> input_password.error = error })

        viewModel.eventLoginError.observe(this, Observer<Boolean> { hasError -> if (hasError) showLoginError() })

        viewModel.eventLoginCorrect.observe(this, Observer<Boolean> { isSuccessful -> if (isSuccessful){
            openTrips()
        } })
    }

    private fun openTrips() {
        val intent = Intent(this, TripsActivity::class.java)
        startActivity(intent)
    }

    private fun showLoginError() {
        Toast.makeText(applicationContext, "Login error!", Toast.LENGTH_LONG).show()
    }
}