package com.example.kotlin_change.data.models

class Media {
    var file: String? = null
    var thumbnail: String? = null
    var midsize: String? = null
    var fullsize: String? = null
    var created_at: String? = null
}