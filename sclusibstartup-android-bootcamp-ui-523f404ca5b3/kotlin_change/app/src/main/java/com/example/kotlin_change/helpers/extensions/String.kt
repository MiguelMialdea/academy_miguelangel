package com.example.kotlin_change.helpers.extensions

import com.example.kotlin_change.helpers.Constants

fun String.isValidEmail() = matches(Constants.EMAIL_PATTERN.toRegex())