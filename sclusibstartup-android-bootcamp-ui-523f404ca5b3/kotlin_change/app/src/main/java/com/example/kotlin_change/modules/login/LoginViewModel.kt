package com.example.kotlin_change.modules.login

import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.room.Room
import com.example.kotlin_change.App
import com.example.kotlin_change.api.Config
import com.example.kotlin_change.api.RetrofitClient
import com.example.kotlin_change.data.local.AppDatabase
import com.example.kotlin_change.data.local.DBLogin
import com.example.kotlin_change.data.models.Login
import com.example.kotlin_change.helpers.Constants
import com.example.kotlin_change.helpers.extensions.isValidEmail
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response

class LoginViewModel : ViewModel() {

    val retrofitClient: RetrofitClient =
        RetrofitClient()
    val username = MutableLiveData<String>()
    val password = MutableLiveData<String>()
    val usernameError = MutableLiveData<String>()
    val passwordError = MutableLiveData<String>()

    // Login button
    var eventLoginError = MutableLiveData<Boolean>()

    // Error on login
    var eventLoginCorrect = MutableLiveData<Boolean>()

    fun postLogin() {
        val login = Login()
        login.client_id = Config.CLIENT_ID
        login.client_secret = Config.CLIENT_SECRET
        login.grant_type = Config.GRANT_TYPE_LOGIN
        login.username = username.value
        login.password = password.value

            CoroutineScope(Dispatchers.IO).launch {
                val responseLogin: Response<Login> = retrofitClient.postLogin(login)
                if (responseLogin.code() == Constants.SERVER_SUCCESS_CODE) {

                    /**
                     * Room working example
                     */
                    val db = Room.databaseBuilder(
                        App.instance,
                        AppDatabase::class.java, "app-database.db"
                    ).build()
                    var dbLogin = DBLogin(1, "", "", "", "", "", "", "")
                    dbLogin!!.access_token = responseLogin.body()?.access_token.toString()
                    db.loginDao().insert(dbLogin)
                    db.loginDao().getLogin()

                    App.preferences.setAccessToken(responseLogin.body()?.access_token)
                    App.preferences.setRefreshToken(responseLogin.body()?.refresh_token)
                    getMe()
                }
            }
        }



    private fun getMe() {
            CoroutineScope(Dispatchers.IO).launch {
                val responseMe = retrofitClient.getMe()
                if (responseMe.code() == Constants.SERVER_SUCCESS_CODE) {
                    withContext(Dispatchers.Main) {

                        App.preferences.setUserId(responseMe.body()?.id)
                        eventLoginCorrect.value = true
                    }
                }
            }
        }

    fun checkLogin() {
        val textUsername: String? = username.value
        val textPassword: String? = password.value

        eventLoginError.value = false
        if (textUsername.isNullOrEmpty()) {
            eventLoginError.value = true
            usernameError.value = "Este campo no puede ser vacío"
        } else if (!username.value!!.isValidEmail()) {
            eventLoginError.value = true
            usernameError.value = "Formato de émail no válido"
        }
        if (textPassword.isNullOrEmpty()) {
            eventLoginError.value = true
            passwordError.value = "Este campo no puede ser vacío"
        }
        if (!eventLoginError.value!!) {
            postLogin()
        }
    }
}