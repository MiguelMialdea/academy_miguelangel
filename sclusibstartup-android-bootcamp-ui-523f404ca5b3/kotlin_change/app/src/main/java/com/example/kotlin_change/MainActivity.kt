package com.example.kotlin_change

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val adapter by lazy { ViewPagerAdapter(this)}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        pager.adapter = adapter

        val tabLayoutMediator = TabLayoutMediator(tab_layout, pager, TabLayoutMediator.TabConfigurationStrategy{tab, position ->
            when (position + 1){
                1 -> {
                    tab.text = "Opcion 1"
                    tab.setIcon(R.drawable.home)
                }
                2 -> {
                    tab.text = "Opcion 2"
                    tab.setIcon(R.drawable.like)
                }
                3 -> {
                    tab.text = "Opcion 3"
                    tab.setIcon(R.drawable.search)
                }
            }
        })
        tabLayoutMediator.attach()


    }
}