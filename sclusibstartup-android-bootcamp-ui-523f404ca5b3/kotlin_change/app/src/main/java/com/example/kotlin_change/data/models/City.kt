package com.example.kotlin_change.data.models

open class City {
    var id: Int = 0
    var place_id: String? = null
    var name: String? = null

    //    private var country: Country? = null
    var medias: Media? = null
//    private var photoMetadata: PhotoMetadata? = null
}