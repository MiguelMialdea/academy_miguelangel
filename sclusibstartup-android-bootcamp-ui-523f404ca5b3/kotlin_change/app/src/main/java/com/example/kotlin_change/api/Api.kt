package com.example.kotlin_change.api

import android.database.Observable
import com.example.kotlin_change.data.models.Chats
import com.example.kotlin_change.data.models.City
import com.example.kotlin_change.data.models.Login
import com.example.kotlin_change.data.models.User

import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST


interface Api {

    @GET("/rates")
    fun getRates(): Observable<List<String>>

    //LOGIN
    @POST("auth/token/")
    suspend fun postLogin(@Body login: Login): Response<Login>

    //GET ME
    @GET("users/me/")
    suspend fun getMe(): Response<User>

    //GET CITIES
    @GET("cities/")
    suspend fun getCities(): Response<Pager<City>>

    //GET CHATS
    @GET("chats/")
    suspend fun getChats(): Response<Chats>

}