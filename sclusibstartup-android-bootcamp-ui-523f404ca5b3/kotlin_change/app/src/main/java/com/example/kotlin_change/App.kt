package com.example.kotlin_change

import android.app.Application
import com.example.kotlin_change.helpers.AppPreferences
import io.realm.Realm
import io.realm.RealmConfiguration

class App : Application() {

    companion object {
        lateinit var preferences: AppPreferences
        lateinit var instance: App private set
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        preferences = AppPreferences(applicationContext)
        Realm.init(this)
        val config = RealmConfiguration.Builder().name("project.realm").build()
        Realm.setDefaultConfiguration(config)

    }
}