package com.example.kotlin_change.modules.trips

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.kotlin_change.api.RetrofitClient
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class TripsViewModel: ViewModel() {

    private val retrofitClient: RetrofitClient =
        RetrofitClient()
    var cities = MutableLiveData<ArrayList<Any>>()

    //Trip onClick
    var navigateToTripDetail = MutableLiveData<Int>()

    init {
        getTrips()
    }

    private fun getTrips() {
        CoroutineScope(Dispatchers.IO).launch {
            val responseTrips = retrofitClient.getCities()
            withContext(Dispatchers.Main) {
                if (responseTrips.isSuccessful) {
                    cities.value = responseTrips.body()?.results as ArrayList<Any>?
                }
            }
        }
    }

}