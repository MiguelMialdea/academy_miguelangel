package com.example.kotlin_change.data.local

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class DBLogin constructor(
    @PrimaryKey var id: Int = 0,
    var access_token: String,
    var client_id: String,
    var client_secret: String,
    var grant_type: String,
    var refresh_token: String,
    var username: String,
    var password: String
)

@Entity
data class DBUser(
    @PrimaryKey var id: Int,
    var name: String
)

