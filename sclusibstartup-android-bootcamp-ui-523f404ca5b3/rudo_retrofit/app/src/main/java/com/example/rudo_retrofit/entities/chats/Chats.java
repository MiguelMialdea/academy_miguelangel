package com.example.rudo_retrofit.entities.chats;

import java.io.Serializable;

public class Chats implements Serializable {

    private int count;

    public Chats(int chats) {
        this.count = chats;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

}
