package com.example.rudo_retrofit.api;

import com.example.rudo_retrofit.entities.City;
import com.example.rudo_retrofit.entities.Trips;
import com.example.rudo_retrofit.entities.chats.Chats;
import com.example.rudo_retrofit.entities.Login;
import com.example.rudo_retrofit.entities.Profile;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public abstract class DataStrategy {

    public abstract Call<Token> refreshToken();

    public abstract void getFirebaseToken(InteractDispatcherGeneric interactDispatcherGeneric);

    public abstract void postRegister(Profile profile, InteractDispatcherObject interactDispatcherObject);

    public abstract void postLogin(Login login, InteractDispatcherObject interactDispatcherObject);

    public abstract void getMe(InteractDispatcherObject interactDispatcherObject);

    public abstract void getChats(InteractDispatcherObject interactDispatcherObject);

    public abstract void getTrips(InteractDispatcherObject interactDispatcherObject);

    public abstract void getCities(int page, InteractDispatcherPager interactDispatcherPagerMedia);


    public interface InteractDispatcherObject<T> {
        void response(int code, T object);
    }

    public interface InteractDispatcherListObject<T> {
        void response(int code, List<T> object);
    }

    public interface InteractDispatcherGeneric {
        void response(int code, String message);
    }

    public interface InteractDispatcherPager<T> {
        void response(int code, Pager<T> pager);
    }


    public interface ApiService {

        //REFRESH TOKEN
        @POST("auth/token/")
        Call<Token> refreshToken(@Body Login login);

        //GET FIREBASE TOKEN
        @GET("/users/firebase_token/")
        Call<String> getFirebaseToken();

        //REGISTER
        @POST("auth/register/")
        Call<Profile> postRegister(@Body Profile profile);

        //LOGIN
        @POST("auth/token/")
        Call<Token> postLogin(@Body Login login);

        //GET ME
        @GET("users/me/")
        Call<Profile> getMe();

        //GET CHATS
        @GET("chats/")
        Call<Chats> getChats();

        //GET TRIPS
        @GET("trips/?page=2")
        Call<Trips> getTrips();

        //GET CITIES IMAGES
        @GET("cities/")
        Call<Pager<City>> getCities(
                @Query("page") int page
        );

    }
}