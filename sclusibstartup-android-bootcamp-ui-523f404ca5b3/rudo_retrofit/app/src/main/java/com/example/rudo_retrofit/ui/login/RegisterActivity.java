package com.example.rudo_retrofit.ui.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.rudo_retrofit.MainActivity;
import com.example.rudo_retrofit.R;
import com.example.rudo_retrofit.api.DataStrategy;
import com.example.rudo_retrofit.api.DataWebService;
import com.example.rudo_retrofit.entities.Login;
import com.example.rudo_retrofit.entities.Profile;
import com.example.rudo_retrofit.utils.Constants;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RegisterActivity extends AppCompatActivity {

    Context context;

    @BindView(R.id.check_terms)
    CheckBox check;
    @BindView(R.id.button_login)
    TextView button_login;
    @BindView(R.id.edit_username)
    EditText edit_name;
    @BindView(R.id.edit_email)
    EditText edit_email;
    @BindView(R.id.edit_password)
    EditText edit_password;
    @BindView(R.id.edit_confirm_password)
    EditText edit_confirm;

    Boolean name_correct = false, email_correct = false, password_correct = false, check_correct = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_activity);
        ButterKnife.bind(this);
        context = this;
        
        textUnderlined();

        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                confirmAll();
                if (name_correct == true && email_correct == true && password_correct == true && check_correct == true){
                    callRegister();
                }
            }
        });
    }

    private void callRegister() {
        button_login.setEnabled(false);

        Profile profile = new Profile();
        profile.setUsername(edit_name.getText().toString());
        profile.setEmail(edit_email.getText().toString());
        profile.setPassword(edit_password.getText().toString());
        profile.setPassword_confirm(edit_confirm.getText().toString());

        new DataWebService().postRegister(profile, new DataStrategy.InteractDispatcherObject() {
            @Override
            public void response(int code, Object object) {
                if (code == Constants.SERVER_CREATED_CODE){
                    goToNext();
                } else {
                    toast(code+"");
                    button_login.setEnabled(true);
                }
            }
        });
    }

    private void confirmAll() {
        // Empty edits comprobation
        if (edit_name.getText().toString().isEmpty()){
            edit_name.setError("Inserte datos");
        } else{
            // Minimum length Name
            if (edit_name.length() < 2){
                edit_name.setError("Requiere minimo 2 letras");
            } else {
                name_correct = true;
            }
        }
        if (edit_email.getText().toString().isEmpty())
        {
            edit_email.setError("Inserte datos");
        } else{
            // Email pattern
            emailValidation(edit_email.getText().toString());
        }
        if (edit_password.getText().toString().isEmpty())
        {
            edit_password.setError("Inserte datos");
        } else{
            // Password pattern
            passwordValidation(edit_password.getText().toString());
        }
        if (edit_confirm.getText().toString().isEmpty()){
            edit_confirm.setError("Inserte datos");
        } else
        {
            // Password repeated confirmation
            if (!edit_confirm.getText().toString().equalsIgnoreCase(edit_password.getText().toString())){
                edit_confirm.setError("Las claves no coinciden");
            } else {
                password_correct = true;
            }
        }

        // Checkbox comprobation
        if (!check.isChecked()){
            toast("Debes aceptar los terminos y condiciones");
        } else {
            check_correct = true;
        }
    }

    private void goToNext() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    private void textUnderlined() {
        SpannableString check_text = new SpannableString("Acepto terminos y condiciones");

        check_text.setSpan(new UnderlineSpan(), 0, check.length(), 0);

        check.setText(check_text);
    }

    private void passwordValidation(String password) {
        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,20}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        if (!matcher.matches()){
            edit_password.setError("La password requiere 8 caracteres, 1 mayúscula, 1 minúscula y 1 número");
        }
    }

    private void toast(String message) {
        Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
        toast.show();
    }

    private void emailValidation(String email) {
        String email_pattern =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        +"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        +"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        +"([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(email_pattern, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if(!matcher.matches()){
            edit_email.setError("El email no es valido");
        } else {
            email_correct = true;
        }
    }
}