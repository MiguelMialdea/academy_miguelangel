package com.example.rudo_retrofit.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.rudo_retrofit.R;
import com.example.rudo_retrofit.entities.City;

import java.util.ArrayList;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {
    private ArrayList<City> dataArrayList;

    public MainAdapter(ArrayList<City> dataArrayList){
        this.dataArrayList = dataArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_row_main, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String image;
        String city = dataArrayList.get(position).getName();
        String country = dataArrayList.get(position).getCountry().getName();

        if (dataArrayList.get(position).getMedias().size() != 0 ){
            image = dataArrayList.get(position).getMedias().get(0).getFullsize();
        } else {
            image = "https://images.pexels.com/photos/5769593/pexels-photo-5769593.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940";
        }

        holder.text_city.setText(city);
        holder.text_country.setText(country);
        Glide.with(holder.itemView).load(image).into(holder.image_city);
    }

    @Override
    public int getItemCount() {
        return dataArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView text_city;
        TextView text_country;
        ImageView image_city;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            image_city = itemView.findViewById(R.id.image_city);
            text_city = itemView.findViewById(R.id.text_city);
            text_country = itemView.findViewById(R.id.text_country);
        }
    }
}
