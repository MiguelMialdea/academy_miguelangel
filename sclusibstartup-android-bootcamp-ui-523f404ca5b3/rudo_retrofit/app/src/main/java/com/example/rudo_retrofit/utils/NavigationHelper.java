package com.example.rudo_retrofit.utils;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

public class NavigationHelper {

    // Put here the main fragments of the main activity
    public static final String F_DESTINATIONS = "F_DESTINATIONS";
    public static final String F_CHATLIST = "F_CHATLIST";
    public static final String F_MY_PROFILE = "F_MY_PROFILE";
    public static final String F_PROFILE = "F_PROFILE";
    public static final String F_NOTIFICATIONS = "F_NOTIFICATIONS";
    public static final String F_DETAILTRIP = "F_DETAILTRIP";

    private List<WeakReference<Fragment>> stack = new ArrayList<>();

    public Fragment showFragment(AppCompatActivity activity, Fragment currentFragment, Fragment futureFragment, int fragmentView, String tag) {

        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        if (tag.equals(F_DESTINATIONS) || tag.equals(F_CHATLIST) || tag.equals(F_MY_PROFILE) || tag.equals(F_NOTIFICATIONS)) {
            clearStack();
            if (fragmentManager.findFragmentByTag(tag) != null) {
                fragmentTransaction.hide(currentFragment);
                fragmentTransaction.show(Objects.requireNonNull(fragmentManager.findFragmentByTag(tag)));
            } else {
                if (currentFragment != null) {
                    fragmentTransaction.hide(currentFragment);
                }
                fragmentTransaction.add(fragmentView, futureFragment, tag);
            }
        } else {
            fragmentTransaction.hide(currentFragment);
            stack.add(new WeakReference<>(currentFragment));
            fragmentTransaction.add(fragmentView, futureFragment, tag);
        }

        fragmentTransaction.commit();

        return futureFragment;
    }

    public Fragment backStackFragment(AppCompatActivity activity, Fragment currentFragment) {

        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment lastFragment = null;

        if (stack.isEmpty()) {
            activity.finish();
        } else {
            lastFragment = fragmentManager.findFragmentByTag(stack.get(stack.size() - 1).get().getTag());
            fragmentTransaction.remove(currentFragment);
            fragmentTransaction.show(Objects.requireNonNull(lastFragment));
            fragmentTransaction.commit();
            stack.remove(stack.size() - 1);
        }
        return lastFragment;
    }

    private void clearStack() {
        Iterator<WeakReference<Fragment>> iterator = stack.iterator();

        while (iterator.hasNext()) {
            Fragment fragment = iterator.next().get();
            if (fragment != null)
                if (fragment.getTag() != null)
                    iterator.remove();
        }
    }
}
