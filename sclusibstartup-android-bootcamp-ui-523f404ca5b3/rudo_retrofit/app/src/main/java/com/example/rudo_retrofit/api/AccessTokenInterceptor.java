package com.example.rudo_retrofit.api;

import androidx.annotation.NonNull;

import com.example.rudo_retrofit.App;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class AccessTokenInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        String accessToken = App.preferences.getAccessToken();
        Request request = newRequestWithAccessToken(chain.request(), accessToken);
        return chain.proceed(request);
    }

    @NonNull
    private Request newRequestWithAccessToken(@NonNull Request request, @NonNull String accessToken) {
        return request.newBuilder()
                .header(Config.TYPE_ITEM_AUTHORIZATION, Config.HTTP_CLIENT_AUTHORIZATION + accessToken)
                .build();
    }
}
