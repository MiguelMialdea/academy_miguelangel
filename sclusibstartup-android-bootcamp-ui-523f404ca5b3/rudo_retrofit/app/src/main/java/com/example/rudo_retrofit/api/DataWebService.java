package com.example.rudo_retrofit.api;

import android.util.Log;

import com.android.volley.BuildConfig;
import com.example.rudo_retrofit.App;
import com.example.rudo_retrofit.entities.City;
import com.example.rudo_retrofit.entities.Login;
import com.example.rudo_retrofit.entities.Media;
import com.example.rudo_retrofit.entities.Profile;
import com.example.rudo_retrofit.entities.Trips;
import com.example.rudo_retrofit.entities.chats.Chats;
import com.example.rudo_retrofit.utils.Constants;
import com.ihsanbal.logging.Level;
import com.ihsanbal.logging.LoggingInterceptor;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.internal.platform.Platform;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class DataWebService extends DataStrategy {


    private Retrofit retrofit;
    private Retrofit retrofitLogin;
    private ApiService apiService;
    private ApiService apiserviceLogin;
    private String TAG_FAILURE = "FAILURE_CALL";

    public DataWebService() {

        OkHttpClient clientWithoutAuth = new OkHttpClient.Builder()
                .addInterceptor(new LoggingInterceptor.Builder()
                        .loggable(BuildConfig.DEBUG)
                        .setLevel(Level.BODY)
                        .log(Platform.INFO)
                        .request("Request")
                        .response("Response")
                        .build())
                .build();

        OkHttpClient clientWithAuth = new OkHttpClient.Builder()
                .addInterceptor(new LoggingInterceptor.Builder()
                        .loggable(BuildConfig.DEBUG)
                        .setLevel(Level.BODY)
                        .log(Platform.INFO)
                        .request("Request")
                        .response("Response")
                        .build())
                .addInterceptor(new AccessTokenInterceptor())
                .authenticator(new AccessTokenAuthenticator())
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES)
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(Config.API_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(clientWithAuth)
                .build();

        apiService = retrofit.create(ApiService.class);

        retrofitLogin = new Retrofit.Builder()
                .baseUrl(Config.API_URL)
                .client(clientWithoutAuth)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        apiserviceLogin = retrofitLogin.create(ApiService.class);

    }

    @Override
    public Call<Token> refreshToken() {
        ApiService apiServiceRefresh = retrofitLogin.create(ApiService.class);

        Login login = new Login();
        login.setRefresh_token(App.preferences.getRefreshToken());
        login.setGrant_type(Config.GRANT_TYPE);

        return apiServiceRefresh.refreshToken(login);
    }

    @Override
    public void getFirebaseToken(InteractDispatcherGeneric interactDispatcherGeneric) {
        apiService.getFirebaseToken().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                interactDispatcherGeneric.response(response.code(), response.body());
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                interactDispatcherGeneric.response(Constants.SERVER_TIMEOUT_CODE, null);
                Log.e(TAG_FAILURE, "onFailure: get firebase token - " + t.getMessage());
            }
        });
    }

    @Override
    public void postRegister(Profile profile, InteractDispatcherObject interactDispatcherObject) {
        apiserviceLogin.postRegister(profile).enqueue(new Callback<Profile>() {
            @Override
            public void onResponse(Call<Profile> call, Response<Profile> response) {
                interactDispatcherObject.response(response.code(), response.body());
            }

            @Override
            public void onFailure(Call<Profile> call, Throwable t) {
                interactDispatcherObject.response(Constants.SERVER_TIMEOUT_CODE, null);
                Log.e(TAG_FAILURE, "onFailure: post register - " + t.getMessage());
            }
        });
    }

    @Override
    public void postLogin(Login login, InteractDispatcherObject interactDispatcherObject) {
        apiserviceLogin.postLogin(login).enqueue(new Callback<Token>() {
            @Override
            public void onResponse(Call<Token> call, Response<Token> response) {
                interactDispatcherObject.response(response.code(), response.body());
            }

            @Override
            public void onFailure(Call<Token> call, Throwable t) {
                interactDispatcherObject.response(Constants.SERVER_TIMEOUT_CODE, null);
                Log.e(TAG_FAILURE, "onFailure: post login - " + t.getMessage());
            }
        });
    }

    @Override
    public void getMe(InteractDispatcherObject interactDispatcherObject) {
        apiService.getMe().enqueue(new Callback<Profile>() {
            @Override
            public void onResponse(Call<Profile> call, Response<Profile> response) {
                interactDispatcherObject.response(response.code(), response.body());
            }

            @Override
            public void onFailure(Call<Profile> call, Throwable t) {
                Log.e(TAG_FAILURE, "onFailure: get me - " + t.getMessage());
            }
        });
    }

    @Override
    public void getChats(InteractDispatcherObject interactDispatcherObject) {
        apiService.getChats().enqueue(new Callback<Chats>() {
            @Override
            public void onResponse(Call<Chats> call, Response<Chats> response) {
                interactDispatcherObject.response(response.code(), response.body());
            }

            @Override
            public void onFailure(Call<Chats> call, Throwable t) {
                Log.e(TAG_FAILURE, "onFailure: get trips - " + t.getMessage());
            }
        });
    }

    @Override
    public void getTrips(InteractDispatcherObject interactDispatcherObject) {
        apiService.getTrips().enqueue(new Callback<Trips>() {
            @Override
            public void onResponse(Call<Trips> call, Response<Trips> response) {
                interactDispatcherObject.response(response.code(), response.body());
            }

            @Override
            public void onFailure(Call<Trips> call, Throwable t) {
                Log.e(TAG_FAILURE, "onFailure: get trips - " + t.getMessage());
            }
        });
    }

    @Override
    public void getCities(int page, InteractDispatcherPager interactDispatcherPagerMedia) {
        apiService.getCities(page).enqueue(new Callback<Pager<City>>() {
            @Override
            public void onResponse(Call<Pager<City>> call, Response<Pager<City>> response) {
                interactDispatcherPagerMedia.response(response.code(), response.body());
            }

            @Override
            public void onFailure(Call<Pager<City>> call, Throwable t) {
                Log.e(TAG_FAILURE, "onFailure: get medias - " + t.getMessage());
            }
        });
    }
}
