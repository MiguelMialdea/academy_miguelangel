package com.example.rudo_retrofit.ui.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.rudo_retrofit.App;
import com.example.rudo_retrofit.MainActivity;
import com.example.rudo_retrofit.R;
import com.example.rudo_retrofit.api.Config;
import com.example.rudo_retrofit.api.DataStrategy;
import com.example.rudo_retrofit.api.DataWebService;
import com.example.rudo_retrofit.api.Token;
import com.example.rudo_retrofit.entities.Login;
import com.example.rudo_retrofit.entities.Profile;
import com.example.rudo_retrofit.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity {

    Context context;

    @BindView(R.id.btnLogin)
    Button button_login;
    @BindView(R.id.txtRemember)
    TextView text_remember;
    @BindView(R.id.txtNewAccount)
    TextView text_newAccount;
    @BindView(R.id.text_new)
    TextView text_new;
    @BindView(R.id.etUsername)
    EditText text_user;
    @BindView(R.id.etpassword)
    EditText text_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        context = this;

        textUnderlined();
        setOnClickListeners();
    }

    private void setOnClickListeners() {
        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callLogin();
            }
        });

        text_remember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toast("Recordar contraseña");
            }
        });

        text_newAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toast("Crear nueva cuenta");
            }
        });
    }

    private void callLogin() {
        button_login.setEnabled(false);

        Login login = new Login();
        login.setUsername(text_user.getText().toString());
        login.setPassword(text_password.getText().toString());
        login.setGrant_type(Config.GRANT_TYPE_LOGIN);

        new DataWebService().postLogin(login, new DataStrategy.InteractDispatcherObject() {
            @Override
            public void response(int code, Object object) {
                if (code == Constants.SERVER_SUCCESS_CODE) {
                    Token token = (Token) object;
                    String key = token.getAccess_token();
                    toast(""+key);
                    App.preferences.setAccessToken(key);
                    goToMainActivity();
                } else {
                    Toast.makeText(context, "Invalid Login", Toast.LENGTH_LONG).show();
                    button_login.setEnabled(true);
                }
            }
        });
    }

    private void goToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    private void textUnderlined() {
        SpannableString text_remember_underlined = new SpannableString("¿No recuerdas tu contraseña?");
        SpannableString text_newAccount_underlined = new SpannableString("¿Crea tu nueva cuenta?");
        SpannableString text_new_underlined = new SpannableString("¿Eres nuevo?");

        text_remember_underlined.setSpan(new UnderlineSpan(), 0, text_remember_underlined.length(), 0);
        text_newAccount_underlined.setSpan(new UnderlineSpan(), 0, text_newAccount_underlined.length(),0);
        text_new_underlined.setSpan(new UnderlineSpan(), 0, text_new_underlined.length(), 0);

        text_remember.setText(text_remember_underlined);
        text_newAccount.setText(text_newAccount_underlined);
        text_new.setText(text_new_underlined);
    }

    public void toast(String message){
        Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
        toast.show();
    }

}
