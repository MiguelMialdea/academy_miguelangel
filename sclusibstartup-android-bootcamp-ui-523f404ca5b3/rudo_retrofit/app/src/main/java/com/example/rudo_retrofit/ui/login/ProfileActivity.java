package com.example.rudo_retrofit.ui.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.example.rudo_retrofit.MainActivity;
import com.example.rudo_retrofit.R;
import com.example.rudo_retrofit.api.DataStrategy;
import com.example.rudo_retrofit.api.DataWebService;
import com.example.rudo_retrofit.api.Pager;
import com.example.rudo_retrofit.entities.City;
import com.example.rudo_retrofit.entities.Media;
import com.example.rudo_retrofit.entities.Profile;
import com.example.rudo_retrofit.entities.chats.Chats;
import com.example.rudo_retrofit.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ProfileActivity extends AppCompatActivity {

    Context context;

    @BindView(R.id.text_trips)
    TextView text_trips;
    @BindView(R.id.text_chats)
    TextView text_chats;
    @BindView(R.id.text_username)
    TextView text_username;
    @BindView(R.id.text_email)
    TextView text_email;
    @BindView(R.id.text_first_name)
    TextView text_password;
    @BindView(R.id.txt_last_name)
    TextView text_password_confirm;
    @BindView(R.id.button_data)
    Button button_data;
    @BindView(R.id.image_guru)
    ImageView image;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        context = this;

        button_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callGetMe();
                callChats();
                //callTrips();
                //callMedia();
            }
        });
    }

    /*private void callMedia() {
        new DataWebService().getCities(0, new DataStrategy.InteractDispatcherPager<City>() {
            @Override
            public void response(int code, Pager<City> pager) {
                if (code == Constants.SERVER_SUCCESS_CODE){
                    Pager<City> city = pager;
                    text_trips.setText(city.getResults().get(1).getName());
                    Glide.with(ProfileActivity.this).load(city.getResults().get(1).getMedias().get(0).getFullsize()).into(image);
                } else {
                    Toast.makeText(context, ""+code, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }*/
/*
    private void callTrips() {
        new DataWebService().getTrips(new DataStrategy.InteractDispatcherObject() {
            @Override
            public void response(int code, Object object) {
                if (code == Constants.SERVER_SUCCESS_CODE) {
                    Trips trips = (Trips) object;
                    int contador = trips.getCount();
                    text_trips.setText(contador+"");
                } else {
                    Toast.makeText(context, ""+code, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

 */

    private void callChats() {
        new DataWebService().getChats(new DataStrategy.InteractDispatcherObject() {
            @Override
            public void response(int code, Object object) {
                if (code == Constants.SERVER_SUCCESS_CODE){
                    Chats chats = (Chats) object;
                    int contador = chats.getCount();
                    text_chats.setText(contador+"");
                } else {
                    Toast.makeText(context, ""+code, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void callGetMe() {
        new DataWebService().getMe(new DataStrategy.InteractDispatcherObject() {
            @Override
            public void response(int code, Object object) {
                if (code == Constants.SERVER_SUCCESS_CODE){
                    Profile profile = (Profile) object;
                    String username = profile.getUsername();
                    String email = profile.getEmail();
                    String firstName = profile.getFirst_name();
                    String lastName = profile.getLast_name();

                    text_username.setText(username);
                    text_email.setText(email);
                    text_password.setText(firstName);
                    text_password_confirm.setText(lastName);


                    //Toast.makeText(context, "Todo optimo : "+code, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, "Crash : "+code, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void goToMain() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
