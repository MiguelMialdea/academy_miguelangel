package com.example.rudo_retrofit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.rudo_retrofit.adapter.MainAdapter;
import com.example.rudo_retrofit.api.DataStrategy;
import com.example.rudo_retrofit.api.DataWebService;
import com.example.rudo_retrofit.api.Pager;
import com.example.rudo_retrofit.entities.City;
import com.example.rudo_retrofit.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    NestedScrollView nestedScrollView;
    ProgressBar progressBar;
    RecyclerView recyclerView;
    ArrayList<City> dataArrayList;
    MainAdapter adapter;
    ImageView image;
    TextView text_city, text_country;
    Button button;
    int page;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeVariables();

        callCities(page);

        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY == v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight()){
                    page++;
                    callCities(page);
                    nestedScrollView.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void initializeVariables() {
        nestedScrollView = findViewById(R.id.scroll_view);
        progressBar = findViewById(R.id.progress_bar);
        dataArrayList = new ArrayList<>();
        recyclerView = findViewById(R.id.recycler_view);
        text_city = findViewById(R.id.text_city);
        text_country = findViewById(R.id.text_country);
        button = findViewById(R.id.button_inicio);
        image = findViewById(R.id.image_new);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void setAdapter() {
        adapter = new MainAdapter(dataArrayList);
        recyclerView.setAdapter(adapter);
    }

    private void callCities(int page) {
        new DataWebService().getCities(page,  new DataStrategy.InteractDispatcherPager<City>() {
            @Override
            public void response(int code, Pager<City> pager) {
                if (code == Constants.SERVER_SUCCESS_CODE) {
                    progressBar.setVisibility(View.GONE);
                    dataArrayList.addAll(pager.getResults());
                    setAdapter();
                    Toast.makeText(MainActivity.this, ""+dataArrayList.size(), Toast.LENGTH_SHORT).show();
                    adapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(MainActivity.this, ""+code, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}