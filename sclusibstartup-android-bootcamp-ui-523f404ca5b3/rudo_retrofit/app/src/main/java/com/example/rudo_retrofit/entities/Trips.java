package com.example.rudo_retrofit.entities;

import java.io.Serializable;

public class Trips implements Serializable {
    private int count;
    private City[] results;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
