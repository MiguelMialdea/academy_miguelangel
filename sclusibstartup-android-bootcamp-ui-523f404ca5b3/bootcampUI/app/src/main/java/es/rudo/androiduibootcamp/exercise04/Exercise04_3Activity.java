package es.rudo.androiduibootcamp.exercise04;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import es.rudo.androiduibootcamp.R;

public class Exercise04_3Activity extends AppCompatActivity {

    String fullName;
    TextView text_fullName;
    Button button_finish;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise04_3);
        text_fullName = findViewById(R.id.text_fullname);
        button_finish = findViewById(R.id.btnFinish);

        fullName = getIntent().getStringExtra("COMPLETE_NAME");
        text_fullName.setText(fullName);

        setOnClickListeners();
    }

    private void setOnClickListeners() {
        button_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent2 = new Intent(getApplicationContext(), Exercise04_1Activity.class);
                intent2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent2.putExtra("EXIT", true);
                startActivity(intent2);
            }
        });
    }
}
