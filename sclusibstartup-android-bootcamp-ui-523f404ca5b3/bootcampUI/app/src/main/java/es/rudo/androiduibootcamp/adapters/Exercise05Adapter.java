package es.rudo.androiduibootcamp.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;


import es.rudo.androiduibootcamp.R;
import es.rudo.androiduibootcamp.entities.User;

public class Exercise05Adapter extends RecyclerView.Adapter<Exercise05Adapter.UserVH> {
    private ArrayList<User> listUsers;
    Boolean isColor = true;

    public Exercise05Adapter(ArrayList<User> listUsers) {
        this.listUsers = listUsers;
    }

    @NonNull
    @Override
    public UserVH onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_search_user, null, false);
        return new UserVH(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final UserVH userVH, final int position) {
        userVH.text.setText(listUsers.get(position).getName());
        userVH.image.setImageResource(listUsers.get(position).getImage());

        userVH.text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        userVH.button_follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isColor){
                    userVH.button_follow.setText("seguido");
                    userVH.button_follow.setBackgroundResource(R.drawable.button_checked);
                    isColor = false;
                } else {
                    userVH.button_follow.setText("seguir");
                    userVH.button_follow.setBackgroundResource(R.drawable.button_selected);
                    isColor = true;
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return listUsers.size();
    }

    public class UserVH extends RecyclerView.ViewHolder {
        TextView text;
        ImageView image;
        Button button_follow;

        public UserVH(@NonNull View row) {
            super(row);
            text = row.findViewById(R.id.text_username);
            image = row.findViewById(R.id.image_user);
            button_follow = row.findViewById(R.id.button_follow);
        }
    }
}
