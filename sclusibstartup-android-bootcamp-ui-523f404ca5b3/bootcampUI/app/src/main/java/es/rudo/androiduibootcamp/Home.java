package es.rudo.androiduibootcamp;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import es.rudo.androiduibootcamp.Exercise06.Exercise06Activity;
import es.rudo.androiduibootcamp.Exercise07.Exercise07Activity;
import es.rudo.androiduibootcamp.exercise01.Exercise01Activity;
import es.rudo.androiduibootcamp.exercise02.Excercise02Activity;
import es.rudo.androiduibootcamp.exercise03.Exercise03Activity;
import es.rudo.androiduibootcamp.exercise04.Exercise04_1Activity;
import es.rudo.androiduibootcamp.exercise05.Exercise05Activity;

public class Home extends AppCompatActivity {

    Button button_ex1, button_ex2, button_ex3, button_ex4, button_ex5, button_ex6, button_ex7;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        button_ex1 = findViewById(R.id.button_ex1);
        button_ex2 = findViewById(R.id.button_ex2);
        button_ex3 = findViewById(R.id.button_ex3);
        button_ex4 = findViewById(R.id.button_ex4);
        button_ex5 = findViewById(R.id.button_ex5);
        button_ex6 = findViewById(R.id.button_ex6);
        button_ex7 = findViewById(R.id.button_ex7);

        buttonListeners();
    }

    private void buttonListeners() {

        button_ex1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Home.this, Exercise01Activity.class);
                startActivity(intent);
            }
        });

        button_ex2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Home.this, Excercise02Activity.class);
                startActivity(intent);
            }
        });

        button_ex3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Home.this, Exercise03Activity.class);
                startActivity(intent);
            }
        });

        button_ex4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Home.this, Exercise04_1Activity.class);
                startActivity(intent);
            }
        });

        button_ex5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Home.this, Exercise05Activity.class);
                startActivity(intent);
            }
        });

        button_ex6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Home.this, Exercise06Activity.class);
                startActivity(intent);
            }
        });

        button_ex7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Home.this, Exercise07Activity.class);
                startActivity(intent);
            }
        });
    }


}