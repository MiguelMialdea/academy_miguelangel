package es.rudo.androiduibootcamp.Exercise06;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Switch;

import es.rudo.androiduibootcamp.R;

public class Exercise06Activity extends AppCompatActivity {

    EditText edit_name;
    CheckBox check_age;
    Switch switch_ticket;
    Button button_save, button_delete;
    SharedPreferences shared;
    public static final String myPrefence = "mypref";
    public static final String name = "name";
    public static final String check = "check";
    public static final String sw = "switch";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise06);
        edit_name = findViewById(R.id.edit_name);
        check_age = findViewById(R.id.check_Age);
        switch_ticket = findViewById(R.id.sw_ticket);
        button_save = findViewById(R.id.btn_save);
        button_delete = findViewById(R.id.btn_delete);

        shared = getSharedPreferences(myPrefence, Context.MODE_PRIVATE);


    }

    public void save(View view){
        String n = edit_name.getText().toString();
        Boolean a = check_age.isChecked();
        Boolean b = switch_ticket.isChecked();
        SharedPreferences.Editor editor = shared.edit();
        editor.putString(name, n);
        editor.putBoolean(check, a);
        editor.putBoolean(sw, b);
        editor.commit();
    }

    public void clear(View view){
        edit_name.setText("");
        check_age.setChecked(false);
        switch_ticket.setChecked(false);
    }

    public void read(View view){
        if (shared.contains(name)){
            edit_name.setText(shared.getString(name, ""));
        }

        if (shared.contains(check)){
            check_age.setChecked(shared.getBoolean(check, false));
        }
        if (shared.contains(sw)){
            switch_ticket.setChecked(shared.getBoolean(sw, false));
        }
    }

}
