package es.rudo.androiduibootcamp.exercise05;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import es.rudo.androiduibootcamp.R;
import es.rudo.androiduibootcamp.adapters.Exercise05Adapter;
import es.rudo.androiduibootcamp.entities.User;

public class Exercise05Activity extends AppCompatActivity {

    ArrayList<User> list_users;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise05);

        list_users = new ArrayList<>();
        recyclerView = findViewById(R.id.recycler_users);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        loadUsers();

        setAdapter();
    }

    private void setAdapter() {
        Exercise05Adapter adapter = new Exercise05Adapter(list_users);
        recyclerView.setAdapter(adapter);
    }

    private void loadUsers() {
        list_users.add(new User("Carlos", R.drawable.image1));
        list_users.add(new User("Miguel", R.drawable.image2));
        list_users.add(new User("David", R.drawable.image3));
        list_users.add(new User("Manuel", R.drawable.image4));
        list_users.add(new User("Ricado", R.drawable.image5));
        list_users.add(new User("Francesco", R.drawable.image6));
        list_users.add(new User("Leticia", R.drawable.image7));
        list_users.add(new User("Ruben", R.drawable.image8));
        list_users.add(new User("Andrea", R.drawable.image9));
    }




}
