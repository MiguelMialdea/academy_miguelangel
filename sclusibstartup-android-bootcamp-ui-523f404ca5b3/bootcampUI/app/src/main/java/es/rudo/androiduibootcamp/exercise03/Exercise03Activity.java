package es.rudo.androiduibootcamp.exercise03;

import androidx.annotation.BinderThread;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import es.rudo.androiduibootcamp.R;

public class Exercise03Activity extends AppCompatActivity {

    CheckBox check;
    TextView text_login;
    EditText edit_name;
    EditText edit_email;
    EditText edit_password;
    EditText edit_confirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise03);

        check = findViewById(R.id.check_terms);
        text_login = findViewById(R.id.text_login);
        edit_name = findViewById(R.id.edit_username);
        edit_email = findViewById(R.id.edit_email);
        edit_password = findViewById(R.id.edit_password);
        edit_confirm = findViewById(R.id.edit_confirm_password);

        textUnderlined();

        text_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Empty edits comprobation
                if (edit_name.getText().toString().isEmpty()){
                    edit_name.setError("Inserte datos");
                } else{
                    // Minimum length Name
                    if (edit_name.length() < 2){
                        edit_name.setError("Requiere minimo 2 letras");
                    }
                }
                if (edit_email.getText().toString().isEmpty())
                {
                    edit_email.setError("Inserte datos");
                } else{
                    // Email pattern
                    emailValidation(edit_email.getText().toString());
                }
                if (edit_password.getText().toString().isEmpty())
                {
                    edit_password.setError("Inserte datos");
                } else{
                    // Password pattern
                    passwordValidation(edit_password.getText().toString());
                }
                if (edit_confirm.getText().toString().isEmpty()){
                    edit_confirm.setError("Inserte datos");
                } else
                    {
                    // Password repeated confirmation
                    if (!edit_confirm.getText().toString().equalsIgnoreCase(edit_password.getText().toString())){
                        edit_confirm.setError("Las claves no coinciden");
                    }
                }

                // Checkbox comprobation
                if (check.isChecked() != true){
                    toast("Debes aceptar los terminos y condiciones");
                }

            }
        });
    }

    private void textUnderlined() {
        SpannableString check_text = new SpannableString("Acepto terminos y condiciones");

        check_text.setSpan(new UnderlineSpan(), 0, check.length(), 0);

        check.setText(check_text);
    }

    private void passwordValidation(String password) {
        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,20}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        if (!matcher.matches()){
            edit_password.setError("La password requiere 8 caracteres, 1 mayúscula, 1 minúscula y 1 número");
        }
    }

    private void toast(String message) {
        Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
        toast.show();
    }

    private void emailValidation(String email) {
        String email_pattern =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        +"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        +"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        +"([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(email_pattern, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if(!matcher.matches()){
            edit_email.setError("El email no es valido");
        }
    }
}
