package es.rudo.androiduibootcamp.exercise04;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import es.rudo.androiduibootcamp.R;

public class Exercise04_1Activity extends AppCompatActivity {

    EditText edit_name;
    Button button_save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise04_1);

        if (getIntent().getBooleanExtra("EXIT", false)) {
            finish();
        }

        edit_name = findViewById(R.id.edit_name);
        button_save = findViewById(R.id.button_save);

        setOnClickListeners();
    }

    private void setOnClickListeners() {
        button_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edit_name.getText().toString().isEmpty()){
                    toast();
                }else{
                    Intent intent = new Intent(Exercise04_1Activity.this, Exercise04_2Activity.class);
                    intent.putExtra("NAME",edit_name.getText().toString());
                    startActivity(intent);
                }
            }
        });
    }

    private void toast() {
        Toast toast = Toast.makeText(getApplicationContext(), "Inserta un nombre",Toast.LENGTH_SHORT);
        toast.show();
    }
}
