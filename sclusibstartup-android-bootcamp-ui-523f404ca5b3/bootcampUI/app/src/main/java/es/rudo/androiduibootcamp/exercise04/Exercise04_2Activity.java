package es.rudo.androiduibootcamp.exercise04;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import es.rudo.androiduibootcamp.R;

public class Exercise04_2Activity extends AppCompatActivity {

    String name;
    EditText edit_surname;
    TextView text_name;
    Button button_save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise04_2);

        if (getIntent().getBooleanExtra("EXIT", false)) {
            finish();
        }

        edit_surname = findViewById(R.id.edit_surname);
        text_name = findViewById(R.id.text_name);
        button_save = findViewById(R.id.button_save);

        name = getIntent().getStringExtra("NAME");
        text_name.setText(name);

        setOnClickListeners();
    }

    private void setOnClickListeners() {

        button_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edit_surname.getText().toString().isEmpty()){
                    toast();
                }else{
                    String completeName = name + " " + edit_surname.getText().toString();
                    Intent intent = new Intent(Exercise04_2Activity.this, Exercise04_3Activity.class);
                    intent.putExtra("COMPLETE_NAME", completeName);
                    startActivity(intent);
                }
            }
        });
    }

    private void toast() {
        Toast toast = Toast.makeText(getApplicationContext(), "Inserta un apellido",Toast.LENGTH_SHORT);
        toast.show();
    }
}
