package es.rudo.androiduibootcamp.exercise02;

import android.content.Context;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import es.rudo.androiduibootcamp.R;


public class Excercise02Activity extends AppCompatActivity {


    Button button_login;
    TextView text_remember;
    TextView text_newAccount;
    TextView text_new;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_excercise02);
        button_login = findViewById(R.id.button_login);
        text_remember = findViewById(R.id.text_remember);
        text_newAccount = findViewById(R.id.text_newAccount);
        text_new = findViewById(R.id.text_new);


        textUnderlined();
        setOnClickListeners();
    }

    private void setOnClickListeners() {
        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toast("Sesion iniciada");
            }
        });

        text_remember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toast("Recordar contraseña");
            }
        });

        text_newAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toast("Crear nueva cuenta");
            }
        });
    }

    private void textUnderlined() {
        SpannableString text_remember_underlined = new SpannableString("¿No recuerdas tu contraseña?");
        SpannableString text_newAccount_underlined = new SpannableString("¿Crea tu nueva cuenta?");

        text_remember_underlined.setSpan(new UnderlineSpan(), 0, text_remember_underlined.length(), 0);
        text_newAccount_underlined.setSpan(new UnderlineSpan(), 0, text_newAccount_underlined.length(),0);

        text_remember.setText(text_remember_underlined);
        text_newAccount.setText(text_newAccount_underlined);
    }

    public void toast(String message){
        Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
        toast.show();
    }

}
